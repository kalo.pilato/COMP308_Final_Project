# COMP308_Final_Project - City Explorer
Authors: Michael Millward, Kalo Pilato, Tim Williams

Renders a procedurally generated city with simulated weather effects and shadow mapping (currently not working).

To Run:
Build and compile project as usual.  Execute ./build/bin/CityExplorer

Key Commands:
	View/Navigation:
		'0'	enter full screen mode
		'w'	move forward
		'a' strafe left
		's'	move backward
		'd' strafe right
		'z' zoom in
		'x' zoom out
		left mouse click and drag	rotate facing angle up/down/left/right

	Weather System:
		'1'	toggle rain simulation on/off
		'2'	toggle snow simulation on/off
		'3'	toggle wind simulation on/off
		'<'	decrease weather intensity (rain and snow)
		'>'	increase weather intensity (rain and snow)
		'f'	toggle fog/atmospheric simulation on/off
		'n'	toggle snow/rain perlin noise effect on/off
		'p'	pause/resume weather simulation
		Arrow Keys	change wind direction/intensity

	Other:
		'*'	toggle shadow mapping on/off
		'q'	quit program
