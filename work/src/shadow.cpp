
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

#include "comp308.hpp"
#include "city.hpp"
#include "shaderLoader.hpp"
#include "shadow.hpp"

#include "shader.hpp"
#include "texture.hpp"
#include "vboindexer.hpp"
#include "shaderLoader.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
//using namespace comp308;
using namespace glm;

GLuint shadowMapTexture = 0;
GLuint shadowMapShader = 0;
bool g_useShadowMapShader = false;

bool spotlightOn = true;
bool directLightOn = true;
bool pointLightOn = true;
bool ambientLightOn = true;
GLfloat spot_position[] = { 0, 15, 0, 1 };
GLfloat spot_direction[] = { 0.0, -10.0, 0.0, 1.0 };
GLuint cutoff = 25.0;
GLfloat direct_position[] = { -6.0, -2.0, 1.0, 1.0 };
GLfloat point_position[] = { 0.0f, 2.0f, 3.0f, 1.0f };

// shadow map variables
GLuint shadowMapSize = 1024;
GLuint FramebufferName;
GLuint depthMatrixID;
GLuint vertexbuffer;
GLuint depth_vertexPosition_modelspaceID;
GLuint elementbuffer;
std::vector<unsigned short> indices;
GLuint programID;
GLuint MatrixID;
GLuint ViewMatrixID;
GLuint ModelMatrixID;
GLuint DepthBiasID;
glm::vec3 lightInvDir;
GLuint lightInvDirID;
GLuint shadowTexture;
GLuint shadowTextureID;
GLuint depthTexture;
GLuint ShadowMapID;
GLuint vertexPosition_modelspaceID;
GLuint vertexUVID;
GLuint vertexNormal_modelspaceID;
GLuint uvbuffer;
GLuint normalbuffer;
static const GLfloat g_quad_vertex_buffer_data[] = {
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	-1.0f,  1.0f, 0.0f,
	-1.0f,  1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	1.0f,  1.0f, 0.0f,
};
GLuint quad_programID;
GLuint texID;
GLuint quad_vertexbuffer;
GLuint quad_vertexPosition_modelspaceID;


void initShader(const std::string vertSource, const std::string fragSource) {
	shadowMapShader = comp308::makeShaderProgram(vertSource, fragSource);
}

Shadow::Shadow(std::vector<window> edgeList, GLuint width, GLuint height) {
	edges = edgeList;
	winWidth = width;
	winHeight = height;
	initShadow();
}

void Shadow::setWindowDimensions(int w, int h){
    winWidth = w;
    winHeight = h;
}

void Shadow::initShadow() {

	// Create and compile our GLSL program from the shaders
	initShader("work/res/shaders/DepthRTT.vertexshader", "work/res/shaders/DepthRTT.fragmentshader");

	// Get a handle for our "MVP" uniform
	depthMatrixID = glGetUniformLocation(shadowMapShader, "depthMVP");

	// Get a handle for our buffers
	depth_vertexPosition_modelspaceID = glGetAttribLocation(shadowMapShader, "vertexPosition_modelspace");


	std::vector<comp308::vec3> vertices;
	std::vector<comp308::vec2> uvs;
	std::vector<comp308::vec3> normals;


	for (unsigned int i = 0; i < edges.size(); i++)
	{
		comp308::vec3 verts[4]{ edges[i].w[0],edges[i].w[1],edges[i].w[2],edges[i].w[3] };
		comp308::vec2 uv[4]{ edges[i].uv[0],edges[i].uv[1],edges[i].uv[2],edges[i].uv[3] };
		comp308::vec3 n = edges[i].n;

		if (verts[3].x != -101) {  // is quad
			vertices.push_back(verts[0]);
			vertices.push_back(verts[1]);
			vertices.push_back(verts[2]);
			vertices.push_back(verts[1]);
			vertices.push_back(verts[2]);
			vertices.push_back(verts[3]);

			uvs.push_back(uv[0]);
			uvs.push_back(uv[1]);
			uvs.push_back(uv[2]);
			uvs.push_back(uv[1]);
			uvs.push_back(uv[2]);
			uvs.push_back(uv[3]);

			normals.push_back(n);
			normals.push_back(n);
			normals.push_back(n);
			normals.push_back(n);
			normals.push_back(n);
			normals.push_back(n);
		}
		else { // is triangle
			vertices.push_back(verts[0]);
			vertices.push_back(verts[1]);
			vertices.push_back(verts[2]);
			uvs.push_back(uv[0]);
			uvs.push_back(uv[1]);
			uvs.push_back(uv[2]);
			normals.push_back(n);
			normals.push_back(n);
			normals.push_back(n);
		}
	}

	std::vector<comp308::vec3> indexed_vertices;
	std::vector<comp308::vec2> indexed_uvs;
	std::vector<comp308::vec3> indexed_normals;
	indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);

	// Load it into a VBO

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);


	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);


	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well

	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	// Render to Texture  

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// Depth texture. Slower than a depth buffer, but you can sample it later in your shader

	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, shadowMapSize, shadowMapSize, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);

	// No color output in the bound framebuffer, only depth.
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	glGenBuffers(1, &quad_vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);

	// Create and compile our GLSL program from the shaders
	quad_programID = LoadShaders("work/res/shaders/Passthrough.vertexshader", "work/res/shaders/SimpleTexture.fragmentshader");
	texID = glGetUniformLocation(quad_programID, "texture");

	// Get a handle for our buffers
	quad_vertexPosition_modelspaceID = glGetAttribLocation(quad_programID, "vertexPosition_modelspace");

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("work/res/shaders/ShadowMapping.vertexshader", "work/res/shaders/ShadowMapping.fragmentshader");

	// Get a handle for our "myTextureSampler" uniform
	shadowTextureID = glGetUniformLocation(programID, "myTextureSampler");

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");
	DepthBiasID = glGetUniformLocation(programID, "DepthBiasMVP");
	ShadowMapID = glGetUniformLocation(programID, "shadowMap");

	// Get a handle for our buffers
	vertexPosition_modelspaceID = glGetAttribLocation(programID, "vertexPosition_modelspace");
	vertexUVID = glGetAttribLocation(programID, "vertexUV");
	vertexNormal_modelspaceID = glGetAttribLocation(programID, "vertexNormal_modelspace");

	// Get a handle for our "LightPosition" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");
}

void Shadow::renderShadow() {

	glEnable(GL_CULL_FACE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glDepthFunc(GL_LESS);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Render to our framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	glViewport(0, 0, shadowMapSize, shadowMapSize); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

						 // Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(shadowMapShader);    // TODO: FIX shader

	lightInvDir = glm::vec3(direct_position[0], direct_position[1], direct_position[2]);
	// Compute the MVP matrix from the light's point of view
	glm::mat4 depthProjectionMatrix = glm::ortho<float>(-10, 10, -10, 10, -10, 20);
	glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	// or, for spot light :
	//glm::vec3 lightPos(5, 20, 20);
	//glm::mat4 depthProjectionMatrix = glm::perspective<float>(45.0f, 1.0f, 2.0f, 50.0f);
	//glm::mat4 depthViewMatrix = glm::lookAt(lightPos, lightPos-lightInvDir, glm::vec3(0,1,0));

	glm::mat4 depthModelMatrix = glm::mat4(1.0);
	glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	glUniformMatrix4fv(depthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(depth_vertexPosition_modelspaceID);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		depth_vertexPosition_modelspaceID,  // The attribute we want to configure
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles !
	glDrawElements(
		GL_TRIANGLES,      // mode
		indices.size(),    // count
		GL_UNSIGNED_SHORT, // type
		(void*)0           // element array buffer offset
		);

	glDisableVertexAttribArray(depth_vertexPosition_modelspaceID);


	// Render to the screen
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, winWidth, winHeight); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

						 // Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	// Compute the MVP matrix from keyboard and mouse input
	glm::mat4 ProjectionMatrix;
	glGetFloatv(GL_PROJECTION_MATRIX, &ProjectionMatrix[0].x);
	glm::mat4 ViewMatrix;
	glGetFloatv(GL_MODELVIEW_MATRIX, &ViewMatrix[0].x);
	//	ViewMatrix = glm::lookAt(glm::vec3(14,6,4), glm::vec3(0,1,0), glm::vec3(0,1,0));  
	glm::mat4 ModelMatrix = glm::mat4(1.0);
	glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
		);

	glm::mat4 depthBiasMVP = biasMatrix*depthMVP;

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
	glUniformMatrix4fv(DepthBiasID, 1, GL_FALSE, &depthBiasMVP[0][0]);

	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, shadowTexture);
	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(shadowTextureID, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glUniform1i(ShadowMapID, 1);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(vertexPosition_modelspaceID);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		vertexPosition_modelspaceID,  // The attribute we want to configure
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
		);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(vertexUVID);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		vertexUVID,                   // The attribute we want to configure
		2,                            // size : U+V => 2
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
		);

	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(vertexNormal_modelspaceID);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		vertexNormal_modelspaceID,    // The attribute we want to configure
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
		);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles !
	glDrawElements(
		GL_TRIANGLES,      // mode
		indices.size(),    // count
		GL_UNSIGNED_SHORT, // type
		(void*)0           // element array buffer offset
		);

	glDisableVertexAttribArray(vertexPosition_modelspaceID);
	glDisableVertexAttribArray(vertexUVID);
	glDisableVertexAttribArray(vertexNormal_modelspaceID);


	// Render only on a corner of the window for debug
	//	glViewport(0, 0, 512, 512);

	// Use our shader
	glUseProgram(quad_programID);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	// Set our "renderedTexture" sampler to user Texture Unit 0
	glUniform1i(texID, 0);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(quad_vertexPosition_modelspaceID);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glVertexAttribPointer(
		quad_vertexPosition_modelspaceID, // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	glDisableVertexAttribArray(quad_vertexPosition_modelspaceID);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);

	glUseProgram(0);  // unuse shadow map shader

					  /*********SHADOW MAP **************/

					  //// Cleanup VBO and shader
					  //glDeleteBuffers(1, &vertexbuffer);
					  //glDeleteBuffers(1, &uvbuffer);
					  //glDeleteBuffers(1, &normalbuffer);
					  //glDeleteBuffers(1, &elementbuffer);
					  //glDeleteProgram(programID);
					  //glDeleteProgram(shadowMapShader);
					  //glDeleteProgram(quad_programID);
					  //glDeleteTextures(1, &shadowTexture);

					  //glDeleteFramebuffers(1, &FramebufferName);
					  //glDeleteTextures(1, &depthTexture);
					  //glDeleteBuffers(1, &quad_vertexbuffer);

					  ///*********SHADOW MAP END**************/
}