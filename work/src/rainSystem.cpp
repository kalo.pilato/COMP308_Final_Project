//
//  rainSystem.cpp
//  COMP308_ShaderParticleTest
//
//  Created by Kalo Pilato on 11/10/15.
//
//
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

#include "comp308.hpp"
#include "imageLoader.hpp"
#include "shaderLoader.hpp"
#include "rainSystem.hpp"
#include "texture.hpp"

using namespace std;
using namespace comp308;

RainSystem::RainSystem(){
    initShader("work/res/shaders/particle.vert", "work/res/shaders/particle.frag");
    initParticles();
    pNoise = new PerlinNoise();
}

int RainSystem::getDeadParticle(){
    
    for(int i=LastUsedParticle; i<MaxParticles; i++){
        if (ParticlesContainer[i].life < 0){
            LastUsedParticle = i;
            return i;
        }
    }
    
    for(int i=0; i<LastUsedParticle; i++){
        if (ParticlesContainer[i].life < 0){
            LastUsedParticle = i;
            return i;
        }
    }
    
    return 0; // All particles are taken, override the first one
}

void RainSystem::sortParticles(){
    sort(&ParticlesContainer[0], &ParticlesContainer[MaxParticles]);
}

void RainSystem::initParticles(){
    
    // Vertex shader
    CameraRight_worldspace_ID  = glGetUniformLocation(g_shader, "CameraRight_worldspace");
    CameraUp_worldspace_ID  = glGetUniformLocation(g_shader, "CameraUp_worldspace");
    ViewProjMatrixID = glGetUniformLocation(g_shader, "VP");
    
    // fragment shader
    TextureID  = glGetUniformLocation(g_shader, "myTextureSampler");
    
    // Get a handle for our buffers
    squareVerticesID = glGetAttribLocation(g_shader, "squareVertices");
    xyzsID = glGetAttribLocation(g_shader, "xyzs");
    colorID = glGetAttribLocation(g_shader, "color");
    
    particle_geo_data = new GLfloat[MaxParticles * 4];
    particle_color_data         = new GLubyte[MaxParticles * 4];
    
    for(int i=0; i<MaxParticles; i++){
        ParticlesContainer[i].life = -1.0f;
        ParticlesContainer[i].cameradistance = -1.0f;
    }
    
    Texture = loadDDS("work/res/textures/particle.dds");
    
    // The VBO containing the 4 vertices of the particles.
    // Thanks to instancing, they will be shared by all particles.
    static const GLfloat g_vertex_buffer_data[] = {
        -0.05f, -0.5f, 0.0f,
        0.05f, -0.5f, 0.0f,
        -0.001f,  0.5f, 0.0f,
        0.001f,  0.5f, 0.0f,
    };
    glGenBuffers(1, &billboard_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, billboard_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
    
    // The VBO containing the positions and sizes of the particles
    glGenBuffers(1, &particles_position_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, particles_position_buffer);
    // Initialize with empty (NULL) buffer : it will be updated later, each frame.
    glBufferData(GL_ARRAY_BUFFER, MaxParticles * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
    
    // The VBO containing the colors of the particles
    glGenBuffers(1, &particles_color_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, particles_color_buffer);
    // Initialize with empty (NULL) buffer : it will be updated later, each frame.
    glBufferData(GL_ARRAY_BUFFER, MaxParticles * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);
}

void RainSystem::changeParticleAngle(vec3 wind){
    vec3 v1 = vec3(-0.04f, -0.5f, 0.0f);
    vec3 v2 = vec3(0.04f, -0.5f, 0.0f);
    vec3 v3 = vec3(-0.001f, 0.6f, 0.0f) - wind;
    vec3 v4 = vec3(0.001f, 0.6f, 0.0f) - wind;
    mat3 rotation = rotationMatrix(cameraAngle, vec3(0.0f,1.0f,0.0f));
    v1 = v1 * rotation;
    v2 = v2 * rotation;
    v3 = v3 * rotation;
    v4 = v4 * rotation;
    
    const GLfloat g_vertex_buffer_data[] = {
        v1.x, v1.y, v1.z,
        v2.x, v2.y, v2.z,
        v3.x, v3.y, v3.z,
        v4.x, v4.y, v4.z
    };
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(g_vertex_buffer_data), g_vertex_buffer_data);
}

void RainSystem::initShader(const std::string vertSource, const std::string fragSource) {
    g_shader = makeShaderProgram(vertSource, fragSource);
}

float RainSystem::randomValue(){
    return (rand() / (RAND_MAX + 1.0));
}

mat3 RainSystem::rotationMatrix(float angle, vec3 axis){
    float x = axis.x;
    float y = axis.y;
    float z = axis.z;
    float c = cos(angle);
    float s = sin(angle);
    float t = 1.0 - c;
    return mat3(t * x * x + c, t * x * y - s * z, t * x * z + s * y,
                t * x * y + s * z, t * y * y + c, t * y * z - s * x,
                t * x * z - s * y, t * y * z + s * x, t * z * z + c);
}

vec2 RainSystem::randomPosition(){
    vec3 pos = vec3(-15.0f + randomValue() * 30.0f,
                    0.0,
                    -30.0f + randomValue() * 30.0f);
    vec3 rotatedPos = pos * rotationMatrix((cameraAngle), vec3(0.0, 1.0, 0.0));
    return vec2(rotatedPos.x, rotatedPos.z) + vec2(cameraPosition.x, cameraPosition.z);
}

void RainSystem::generateParticles(double delta){
    // Generate new particles
    int newparticles = (int)(delta * 2000.0 * raindropDensity);
    for(int i=0; i<newparticles; i++){
        int particleIndex = getDeadParticle();
        
        // Set life of particle
        ParticlesContainer[particleIndex].life = lifeTime;
        
        // Set start position of particle
        vec2 randomXZ = randomPosition();
        ParticlesContainer[particleIndex].pos = vec3(randomXZ.x, startHeight, randomXZ.y);
        
        vec3 maindir = vec3(0.0f, -initialSpeed, 0.0f);
        
        
        vec3 randomdir = vec3(randomValue() * randomDirectionScalar,
                              randomValue() * randomDirectionScalar,
                              randomValue() * randomDirectionScalar
                              );
        
        ParticlesContainer[particleIndex].speed = maindir + randomdir;
        
        float particleBrightness = randomValue() / 4.0 + 0.3;
        ParticlesContainer[particleIndex].r = particleBrightness * 256;
        ParticlesContainer[particleIndex].g = particleBrightness * 256;
        ParticlesContainer[particleIndex].b = particleBrightness * 256 + 20;
        ParticlesContainer[particleIndex].a = (randomValue() / 3.0f) * 256;
        
        ParticlesContainer[particleIndex].size = (randomValue() / 5.0f) * raindropSize;
    }
}

int RainSystem::updateParticles(double delta, vec3 cameraPosition, vec3 wind){
    // Update all particles
    int particlesCount = 0;
    for(int i=0; i<MaxParticles; i++){
        
        Particle& p = ParticlesContainer[i];
        
        if(p.life > 0.0f){
            
            // Decrease life
            if(!paused)p.life -= delta;
            if (p.life > 0.0f){
                
                if(!paused){
                    // Simulate particle physics

                    // Create noise vector
                    float noiseFloat = pNoise->noise(p.pos.x, p.pos.y, p.pos.z);
                    vec3 noiseVec(0.0f, 0.0f, 0.0f);
                    if(noiseOn){
                        noiseVec.x = cos(((noiseFloat - 0.3) * pi() * 2.0));
                        noiseVec.y = 0.0f;
                        noiseVec.z = sin(((noiseFloat - 0.3) * pi() * 2.0)) - 0.5;
                        noiseVec *= noiseEffect;
                    }
                    else{
                        p.speed = vec3(0.0f, p.speed.y, 0.0f);
                    }
                    // Calculate final position vertex, applying gravity and adding noise for non-linear descent
                    p.speed += vec3(0.0f,-9.81f, 0.0f) * ((float)delta * p.size) + noiseVec;;
                    p.pos += p.speed * (float)delta;
                    p.pos += wind;
                    p.cameradistance = length( p.pos - cameraPosition );
                }
                
                // Fill the GPU buffer
                particle_geo_data[4*particlesCount+0] = p.pos.x;
                particle_geo_data[4*particlesCount+1] = p.pos.y;
                particle_geo_data[4*particlesCount+2] = p.pos.z;
                
                particle_geo_data[4*particlesCount+3] = p.size;
                
                particle_color_data[4*particlesCount+0] = p.r;
                particle_color_data[4*particlesCount+1] = p.g;
                particle_color_data[4*particlesCount+2] = p.b;
                particle_color_data[4*particlesCount+3] = p.a * (p.cameradistance / 10.0f);
                
            }else{
                // Particles that just died will be put at the end of the buffer in SortParticles();
                p.cameradistance = -1.0f;
            }
            
            particlesCount++;
        }
    }
    return particlesCount;
}

void RainSystem::updateParticleBuffers(int particleCount){
    // Particle Positions
    glBindBuffer(GL_ARRAY_BUFFER, particles_position_buffer);
    glBufferData(GL_ARRAY_BUFFER, MaxParticles * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, particleCount * sizeof(GLfloat) * 4, particle_geo_data);
    
    // Particle Colours
    glBindBuffer(GL_ARRAY_BUFFER, particles_color_buffer);
    glBufferData(GL_ARRAY_BUFFER, MaxParticles * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, particleCount * sizeof(GLubyte) * 4, particle_color_data);
}

void RainSystem::render(vec3 windVector, vec3 camDir){
    if(lastTime == -1) lastTime = glutGet(GLUT_ELAPSED_TIME);
    
    int currentTime = glutGet(GLUT_ELAPSED_TIME);
    int deltaTime = currentTime - lastTime;
    lastTime = currentTime;
    
    double delta = deltaTime/1000.0;

    // Calculate camera angle
    cameraDirection = camDir;
    cameraAngle = atan(cameraDirection.x / cameraDirection.z);
    if(cameraDirection.z > 0){
        if(cameraDirection.x < 0) cameraAngle = pi() / 2.0 + (pi() / 2.0 + cameraAngle);
        else cameraAngle = -(pi() / 2.0) - (pi() / 2.0 - cameraAngle);
    }
    
    mat4 projectionMatrix;
    glGetFloatv(GL_PROJECTION_MATRIX, &projectionMatrix[0].x);
    mat4 viewMatrix;
    glGetFloatv(GL_MODELVIEW_MATRIX, &viewMatrix[0].x);
    cameraPosition = vec3(inverse(viewMatrix)[3]);
    mat4 viewProjectionMatrix = projectionMatrix * viewMatrix;
    
    if(isRaining){
        if(!paused) generateParticles(delta);
    }
    int particleCount = updateParticles(delta, cameraPosition, windVector);
    sortParticles();
    
    updateParticleBuffers(particleCount);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    // Use Shader
    glUseProgram(g_shader);
    
    // Bind Texture
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture);
    // Set Texture Sampler to use Texture 0
    glUniform1i(TextureID, 0);
    
    glUniform3f(CameraRight_worldspace_ID, viewMatrix[0][0], viewMatrix[1][0], viewMatrix[2][0]);
    glUniform3f(CameraUp_worldspace_ID   , viewMatrix[0][1], viewMatrix[1][1], viewMatrix[2][1]);
    
    glUniformMatrix4fv(ViewProjMatrixID, 1, GL_FALSE, &viewProjectionMatrix[0][0]);
    
    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(squareVerticesID);
    glBindBuffer(GL_ARRAY_BUFFER, billboard_vertex_buffer);
    changeParticleAngle(windVector * 4.0);
    glVertexAttribPointer(
                          squareVerticesID,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_FLOAT,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
    // 2nd attribute buffer : positions of particles' centers
    glEnableVertexAttribArray(xyzsID);
    glBindBuffer(GL_ARRAY_BUFFER, particles_position_buffer);
    glVertexAttribPointer(
                          xyzsID,                                // attribute. No particular reason for 1, but must match the layout in the shader.
                          4,                                // size : x + y + z + size => 4
                          GL_FLOAT,                         // type
                          GL_FALSE,                         // normalized?
                          0,                                // stride
                          (void*)0                          // array buffer offset
                          );
    
    // 3rd attribute buffer : particles' colors
    glEnableVertexAttribArray(colorID);
    glBindBuffer(GL_ARRAY_BUFFER, particles_color_buffer);
    glVertexAttribPointer(
                          colorID,                                // attribute. No particular reason for 1, but must match the layout in the shader.
                          4,                                // size : r + g + b + a => 4
                          GL_UNSIGNED_BYTE,                 // type
                          GL_TRUE,                          // normalized?    *** YES, this means that the unsigned char[4] will be accessible with a vec4 (floats) in the shader ***
                          0,                                // stride
                          (void*)0                          // array buffer offset
                          );
    
    // These functions are specific to glDrawArrays*Instanced*.
    // The first parameter is the attribute buffer we're talking about.
    // The second parameter is the "rate at which generic vertex attributes advance when rendering multiple instances"
    // http://www.opengl.org/sdk/docs/man/xhtml/glVertexAttribDivisor.xml
    glVertexAttribDivisorARB(squareVerticesID, 0); // particles vertices : always reuse the same 4 vertices -> 0
    glVertexAttribDivisorARB(xyzsID, 1); // positions : one per quad (its center)                 -> 1
    glVertexAttribDivisorARB(colorID, 1); // color : one per quad                                  -> 1
    
    // Draw the particules !
    // This draws many times a small triangle_strip (which looks like a quad).
    // This is equivalent to :
    // for(i in ParticlesCount) : glDrawArrays(GL_TRIANGLE_STRIP, 0, 4),
    // but faster.
    glDrawArraysInstancedARB(GL_TRIANGLE_STRIP, 0, 4, particleCount);
    
    // Reset array parameters (HACK!!!)
    glVertexAttribDivisorARB(squareVerticesID, 0); // particles vertices : always reuse the same 4 vertices -> 0
    glVertexAttribDivisorARB(xyzsID, 0); // positions : one per quad (its center)                 -> 1
    glVertexAttribDivisorARB(colorID, 0); // color : one per quad                                  -> 1
    
    glDisableVertexAttribArray(squareVerticesID);
    glDisableVertexAttribArray(xyzsID);
    glDisableVertexAttribArray(colorID);
    glDisable(GL_BLEND);
    glUseProgram(0);
}

void RainSystem::startRain(){
    isRaining = true;
}

void RainSystem::stopRain(){
    isRaining = false;
}

void RainSystem::pauseRain(){
    paused = true;
}

void RainSystem::resumeRain(){
    paused = false;
}

void RainSystem::increaseDensity(){
    if(raindropSize < maxSize) raindropSize += 0.15;
    if(raindropDensity <= maxDensity)raindropDensity += 1.0;
}

void RainSystem::decreaseDensity(){
    if(raindropSize > minSize)raindropSize -= 0.25;
    if(raindropDensity > minDensity)raindropDensity -= 0.5;
}

void RainSystem::toggleNoise(){
    noiseOn = !noiseOn;
}

void RainSystem::clear(){
    delete[] particle_geo_data;
    delete[] particle_color_data;
    glDeleteBuffers(1, &particles_color_buffer);
    glDeleteBuffers(1, &particles_position_buffer);
    glDeleteBuffers(1, &billboard_vertex_buffer);
    glDeleteProgram(g_shader);
    glDeleteTextures(1, &Texture);
}