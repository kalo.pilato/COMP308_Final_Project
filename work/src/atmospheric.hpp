//
//  atmospheric.hpp
//  COMP308_a4
//
//  Created by Kalo Pilato on 14/10/15.
//
//

#include "comp308.hpp"
class Atmospheric{
private:
    
    bool atmosOn = true;
    
    float fogColor = 0.5f; // value for all RBG to make grey
    float fogFrames = 300;
    int currentFrame = 0;
    float fogMaxDensity = 0.03;
    float fogMinDensity = 0.008;
    float currentFogDensity = fogMinDensity;
    float targetFogDensity = fogMinDensity;
    float fogIncrement = 0.0f;
    bool isRaining = false;
    bool isSnowing = false;
    
    // Default background colors
    float backgroundR = 0.314f;
    float backgroundG = 0.824f;
    float backgroundB = 1.0f;
    
    // Fog background colors
    float fogBackgroundR = 0.353f;
    float fogBackgroundG = 0.6f;
    float fogBackgroundB = 0.678f;
    
    float rInc = 0.0f;
    float gInc = 0.0f;
    float bInc = 0.0f;
    
    // Current background colors
    float currentR = backgroundR;
    float currentG = backgroundG;
    float currentB = backgroundB;
    
    void initialise();
    
public:
    Atmospheric();   // Constructor
    
    void clearSkyColor();
    
    void setAtmospheric(float density);
    void resetAtmospheric();
    void incrementAtmospheric();
    void decrementAtmospheric();
    void render();
};
