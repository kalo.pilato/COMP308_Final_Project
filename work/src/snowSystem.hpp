//
//  snowSystem.hpp
//  COMP308_Final_Project
//
//  Created by Kalo Pilato on 11/10/15.
//
//

#include <stdio.h>

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "comp308.hpp"
#include "perlinNoise.h"

using namespace comp308;

struct SnowParticle{
    vec3 pos, speed;
    unsigned char r,g,b,a; // Color
    float size, angle, weight;
    float life; // Remaining life of the particle. if <0 : dead and unused.
    float cameradistance; // *Squared* distance to the camera. if dead : -1.0f
    
    bool operator<(const SnowParticle& that) const {
        // Sort in reverse order : far particles drawn first.
        return this->cameradistance > that.cameradistance;
    }
};

class SnowSystem {
private:
    
    static const int MaxParticles = 100000;
    SnowParticle ParticlesContainer[MaxParticles];
    int LastUsedParticle = 0;
    
    PerlinNoise *pNoise;
    float noiseEffect = 1.0 / 40.0;
    bool noiseOn = true;
    
    GLuint CameraRight_worldspace_ID;
    GLuint CameraUp_worldspace_ID;
    GLuint ViewProjMatrixID;
    GLuint TextureID;
    GLuint squareVerticesID;
    GLuint xyzsID;
    GLuint colorID;
    GLfloat* particle_geo_data;
    GLubyte* particle_color_data;
    GLuint Texture;
    GLuint billboard_vertex_buffer;
    GLuint particles_position_buffer;
    GLuint particles_color_buffer;
    GLuint g_shader = 0;
    
    vec3 cameraPosition;
    vec3 cameraDirection;
    float cameraAngle;
    
    int lastTime = -1;
    
    // Snow variables
    float startHeight = 22.0f;
    float lifeTime = 7.0f;
    bool isSnowing = false;
    bool paused = false;
    float snowSize = 0.6f;
    float maxSize = 1.0;
    float minSize = 0.2;
    float snowdropDensity = 4.0f;
    float maxDensity = 8.0f;
    float minDensity = 1.0f;
    float randomDirectionScalar = 0.8f;
    float initialSpeed = 3.0f;
    
    int getDeadParticle();
    void sortParticles();
    void initParticles();
    void initShader(const std::string vertSource, const std::string fragSource);
    void generateParticles(double delta);
    int updateParticles(double delta, vec3 cameraPosition, vec3 wind);
    void updateParticleBuffers(int particleCount);
    float randomValue();
    vec2 randomPosition();
    mat3 rotationMatrix(float angle, vec3 axis);
    void changeParticleAngle(vec3 wind);
    void resetParticleAngle();
    
public:
    SnowSystem();
    
    void render(vec3 wind, vec3 camDir);
    void increaseDensity();
    void decreaseDensity();
    void startSnow();
    void stopSnow();
    void startWind();
    void stopWind();
    void pauseSnow();
    void resumeSnow();
    void toggleNoise();
    void clear();
};
