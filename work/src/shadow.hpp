//
//  Shadow.hpp
//  COMP308_a4
//
//  Created by Tim Williams 15/10/2015
//
//


#include "comp308.hpp"

using namespace comp308;

class Shadow {
private:
	std::vector<window> edges;
	GLuint winWidth;
	GLuint winHeight;

public:
	Shadow(std::vector<window> edges, GLuint width, GLuint height);
	void initShadow();
	void renderShadow();
    void setWindowDimensions(int w, int h);
};