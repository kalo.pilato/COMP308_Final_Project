//
//  wind.cpp
//  COMP308_a4
//
//  Created by Kalo Pilato on 14/10/15.
//
//

#include "wind.hpp"

Wind::Wind(){
    
}

void Wind::setWind(vec3 windVector){
    targetWind = windVector;
    windIncrement = (targetWind - currentWind) / (float)frames;
    currentFrame = 0;
}

vec3 Wind::wind(){
    if(currentFrame != frames){
        currentWind += windIncrement;
        currentFrame++;
    }
    return currentWind;
}

void Wind::reset(){
    targetWind = noWind;
    currentWind = noWind;
    windIncrement = noWind;
}