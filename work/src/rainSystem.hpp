//
//  rainSystem.hpp
//  COMP308_Final_Project
//
//  Created by Kalo Pilato on 11/10/15.
//
//
#include <stdio.h>

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "comp308.hpp"
#include "perlinNoise.h"

using namespace comp308;

struct Particle{
    vec3 pos, speed;
    unsigned char r,g,b,a; // Color
    float size, angle, weight;
    float life; // Remaining life of the particle. if <0 : dead and unused.
    float cameradistance; // *Squared* distance to the camera. if dead : -1.0f
    
    bool operator<(const Particle& that) const {
        // Sort in reverse order : far particles drawn first.
        return this->cameradistance > that.cameradistance;
    }
};

class RainSystem {
private:
    
    static const int MaxParticles = 100000;
    Particle ParticlesContainer[MaxParticles];
    int LastUsedParticle = 0;
    
    GLuint CameraRight_worldspace_ID;
    GLuint CameraUp_worldspace_ID;
    GLuint ViewProjMatrixID;
    GLuint TextureID;
    GLuint squareVerticesID;
    GLuint xyzsID;
    GLuint colorID;
    GLfloat* particle_geo_data;
    GLubyte* particle_color_data;
    GLuint Texture;
    GLuint billboard_vertex_buffer;
    GLuint particles_position_buffer;
    GLuint particles_color_buffer;
    GLuint g_shader = 0;
    
    vec3 cameraPosition;
    vec3 cameraDirection;
    float cameraAngle;
    
    PerlinNoise *pNoise;
    float noiseEffect = 1.0 / 30.0;
    bool noiseOn = true;
    
    int lastTime = -1;
    
    // Rain variables
    float startHeight = 22.0f;
    float lifeTime = 3.0f;
    bool isRaining = false;
    bool paused = false;
    float raindropSize = 1.25f;
    float maxSize = 2.0;
    float minSize = 0.5;
    float raindropDensity = 5.0f;
    float maxDensity = 8.0f;
    float minDensity = 2.0f;
    float randomDirectionScalar = 0.2f;
    float initialSpeed = 5.0f;
    
    int getDeadParticle();
    void sortParticles();
    void initParticles();
    void initShader(const std::string vertSource, const std::string fragSource);
    void generateParticles(double delta);
    int updateParticles(double delta, vec3 cameraPosition, vec3 wind);
    void updateParticleBuffers(int particleCount);
    float randomValue();
    vec2 randomPosition();
    mat3 rotationMatrix(float angle, vec3 axis);
    void changeParticleAngle(vec3 wind);
    void resetParticleAngle();
    
public:
    RainSystem();
    
    void render(vec3 wind, vec3 camDir);
    void increaseDensity();
    void decreaseDensity();
    void startRain();
    void stopRain();
    void startWind();
    void stopWind();
    void pauseRain();
    void resumeRain();
    void toggleNoise();
    void clear();
};
