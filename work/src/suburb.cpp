//
//  suburb.cpp
//  COMP308_a4
//
//  Created by Michael Millward on 4/10/15.
//
//

#include <cmath>
#include <iostream> // input/output streams
#include <fstream>  // file streams
#include <sstream>  // string streams
#include <string>
#include <stdexcept>
#include <vector>
#include <map>

#include "suburb.hpp"
#include "city.hpp"


using namespace std;
using namespace comp308;

/* Changeable fields */
float storyH = 1.5f;
float nWindowsH = 4;

/* Wouldn't recomend changing */
float groundY = 0.0f;
float faceEx = 0.05;

// -------- Residental ----------------------------------------------------------------------------- //

Residental::Residental(vector<lot> lots, int provWTRange[], int provRTRange[], int provWDTRange[]){
    // Take the provided texture range and copy it into local memory
    for(int i = 0; i < 2; i++){
        wallTRange[i] = provWTRange[i];
        roofTRange[i] = provRTRange[i];
        windowTRange[i] = provWDTRange[i];
    }
    subdivideLots(lots);
}

/* Method may have bug in it (see potential bugs) */
void Residental::subdivideLots(std::vector<lot> bigLots){
    //srand(time(NULL));
    // Generate sized lots
    for(unsigned int i = 0; i < bigLots.size(); i++){
        lot bigLot = bigLots[i];
        
        lot subdividedLot;
        
        float lotDividerW = lotDRW * bigLot.width;
        float lotDividerD = lotDRD * bigLot.depth;
        
        float xPointer = bigLot.l[0].x + lotDividerW;
        float zPointer = bigLot.l[0].z - lotDividerD;
        
        
        // Big lot is cut in half depth
        float subLotDepth = ((bigLot.depth-lotDividerD*3)/ 2) ;
        float xMax = xPointer + bigLot.width;
        // While we still have xDistance left
        // Generate both front and back lots (0 = front, 1 = back)
        while(xPointer < xMax){
            float subLotWidth;
            if(xMax - xPointer < (minBWR*bigLot.width) + lotDividerW){
                // Not enough room for another house
                break;
            }
            if(xMax - xPointer < (maxBWR*bigLot.width)+lotDividerW){
                // Deals with the last lot
                subLotWidth = xMax - xPointer - (lotDividerW*2);
            }
            else{
                // Otherwise, create a random width that is between minWidth and maxWidth
                float mxr = maxBWR * bigLot.width;
                float mnr = minBWR * bigLot.width;
                float widthRatio = rand()%100;
                subLotWidth = (mxr - mnr) * (widthRatio/100) + mnr;
            }
            
            vec3 vector1;
            vector1.x = xPointer;
            vector1.y = groundY;
            vector1.z = zPointer;
            subdividedLot.l[0] = vector1;
            vec3 vector2;
            vector2.x = xPointer;
            vector2.y = groundY;
            vector2.z = zPointer-subLotDepth;
            subdividedLot.l[1] = vector2;
            vec3 vector3;
            vector3.x = xPointer+subLotWidth;
            vector3.y = groundY;
            vector3.z = zPointer-subLotDepth;
            subdividedLot.l[2] = vector3;
            vec3 vector4;
            vector4.x = xPointer+subLotWidth;
            vector4.y = groundY;
            vector4.z = zPointer;
            subdividedLot.l[3] = vector4;
        
            subdividedLot.width = subLotWidth;
            subdividedLot.depth = subLotDepth;
        
            // Now increment xPointer
            xPointer = xPointer+subLotWidth+lotDividerW;
            
            float random = rand() % 100;
            if(random < (chanceNL*100)){
                continue;
            }
            
            r_lots.push_back(subdividedLot);
            
        }
        
        // Now lets deal with the lots behind
        xPointer = bigLot.l[1].x + lotDividerW;
        zPointer = zPointer - subLotDepth - lotDividerD;
        
        while(xPointer < xMax){
            float subLotWidth;
            if(xMax - xPointer < (minBWR*bigLot.width) + lotDividerW){
                // Not enough room for another house
                break;
            }
            if(xMax - xPointer < (maxBWR*bigLot.width)+lotDividerW){
                // Deals with the last lot
                subLotWidth = xMax - xPointer - (lotDividerW*2);
            }
            else{
                // Otherwise, create a random width that is between minWidth and maxWidth
                float mxr = maxBWR * bigLot.width;
                float mnr = minBWR * bigLot.width;
                float widthRatio = rand()%100;
                subLotWidth = (mxr - mnr) * (widthRatio/100) + mnr;
            }
            vec3 vector1;
            vector1.x = xPointer;
            vector1.y = groundY;
            vector1.z = zPointer;
            subdividedLot.l[0] = vector1;
            vec3 vector2;
            vector2.x = xPointer;
            vector2.y = groundY;
            vector2.z = zPointer-subLotDepth;
            subdividedLot.l[1] = vector2;
            vec3 vector3;
            vector3.x = xPointer+subLotWidth;
            vector3.y = groundY;
            vector3.z = zPointer-subLotDepth;
            subdividedLot.l[2] = vector3;
            vec3 vector4;
            vector4.x = xPointer+subLotWidth;
            vector4.y = groundY;
            vector4.z = zPointer;
            subdividedLot.l[3] = vector4;
            
            subdividedLot.width = subLotWidth;
            subdividedLot.depth = subLotDepth;
            
            // Now increment xPointer
            xPointer = xPointer+subLotWidth+lotDividerW;
            
            float random = rand() % 100;
            if(random < (chanceNL*100)){
                continue;
            }
            
            r_lots.push_back(subdividedLot);
            
        }
    }
}

void Residental::getLotSize(){
    cout << "residental lots: " << r_lots.size() << "getValue " << getValue() << endl;
}

void Residental::getBuildingDimensions(float *height, float *width, float *depth, lot lot){
    int value = (rand() % 2) + 1;
    (*height) = value * storyH;
    (*width) = lot.width;
    (*depth) = lot.depth;
}

std::vector<lot> Residental::getLots(){
    return r_lots;
}

void Residental::assignBuildingToLot(building building, int lotIndex){
    // Check to make sure index is in bounds
    r_lots[lotIndex].lotBuilding = building;
}

void Residental::calculateEdgeWindows(int *numWindowsHorozontal, float *windowWidth, float *windowHeight, float width){
    // Either 1 window, or no windows
    float value = rand() % 100;
    if(value > (chanceNW*100)){
        (*numWindowsHorozontal) = 1;
        (*windowHeight) = storyH / 2;
        (*windowWidth) = storyH / 2;
    }
    else{
        (*numWindowsHorozontal) = 0;
        (*windowWidth) = 0;
        (*windowHeight) = 0;
    }
}

void Residental::assignTexture(building *build){
    // Wall texture
    if(wallTRange[0] != -1){
        int upperBound = wallTRange[1];
        int lowerLimit = wallTRange[0];
        int range = upperBound - lowerLimit;
        int chosenTexture = (rand() % range) + lowerLimit;
        build->wallTexture = chosenTexture;
    }
    
    // Roof texture
    /*
     int upperBound = textureRange[1];
     int lowerLimit = textureRange[0];
     int chosenTexture = rand() % upperBound;
     build->wallTexture = chosenTexture;
     */
    if(roofTRange[0] != -1){
        int upperBound = roofTRange[1];
        int lowerLimit = roofTRange[0];
        int range = upperBound - lowerLimit;
        int chosenTexture = (rand() % range) + lowerLimit;
        build->roofTexture = chosenTexture;
    }
    
    if(windowTRange[0] != -1){
        int upperBound = windowTRange[1];
        int lowerLimit = windowTRange[0];
        int range = upperBound - lowerLimit;
        int chosenTexture = (rand() % range) + lowerLimit;
        build->windowTexture = chosenTexture;
    }
}

void Residental::generateRoof(float x, float z, float width, float height, float depth, building *build){
    float roofHeight = (2*storyH)/3.0f;
    vec3 vert11;
    vec3 vert12;
    vec3 vert13;
    vec3 normal1;
    vector<vec2> uv1;
    
    // front
    vert11.x = x;
    vert11.y = height;
    vert11.z = z;
    vert12.x = x+(width/2);
    vert12.y = height+roofHeight;
    vert12.z = z;
    vert13.x = x+width;
    vert13.y = height;
    vert13.z = z;
    calculateVectorNormal(vert11, vert12, vert13, &normal1);
    generateTriangleUVPoints(&uv1, width);
    build->sr[0].v[0] = vert11;
    build->sr[0].v[1] = vert12;
    build->sr[0].v[2] = vert13;
    build->sr[0].uv[0] = uv1[0];
    build->sr[0].uv[1] = uv1[1];
    build->sr[0].uv[2] = uv1[2];
    build->sr[0].n = normal1;
    build->sr[0].width = width;
    build->sr[0].height = roofHeight;
    build->sr[0].isTriangle = true;
    
    vec3 vert21;
    vec3 vert22;
    vec3 vert23;
    vec3 vert24;
    vec3 normal2;
    vector<vec2> uv2;
    
    // right
    vert21.x = x+width;
    vert21.y = height;
    vert21.z = z;
    vert22.x = x+(width/2);
    vert22.y = height+roofHeight;
    vert22.z = z;
    vert23.x = x+(width/2);
    vert23.y = height+roofHeight;
    vert23.z = z-depth;
    vert24.x = x+width;
    vert24.y = height;
    vert24.z = z-depth;
    calculateVectorNormal(vert21, vert22, vert23, &normal2);
    generateRoofUVPoints(&uv2, width);
    build->sr[1].v[0] = vert21;
    build->sr[1].v[1] = vert22;
    build->sr[1].v[2] = vert23;
    build->sr[1].v[3] = vert24;
    build->sr[1].uv[0] = uv2[0];
    build->sr[1].uv[1] = uv2[1];
    build->sr[1].uv[2] = uv2[2];
    build->sr[1].uv[3] = uv2[3];
    build->sr[1].n = normal2;
    build->sr[1].width = width;
    build->sr[1].height = height;
    
    vec3 vert31;
    vec3 vert32;
    vec3 vert33;
    vec3 vert34;
    vec3 normal3;
    vector<vec2> uv3;
    
    // left
    vert31.x = x;
    vert31.y = height;
    vert31.z = z-depth;
    vert32.x = x+(width/2);
    vert32.y = height+roofHeight;
    vert32.z = z-depth;
    vert33.x = x+(width/2);
    vert33.y = height+roofHeight;
    vert33.z = z;
    vert34.x = x;
    vert34.y = height;
    vert34.z = z;
    
    calculateVectorNormal(vert31, vert32, vert33, &normal3);
    generateRoofUVPoints(&uv3, width);
    build->sr[2].v[0] = vert31;
    build->sr[2].v[1] = vert32;
    build->sr[2].v[2] = vert33;
    build->sr[2].v[3] = vert34;
    build->sr[2].uv[0] = uv3[0];
    build->sr[2].uv[1] = uv3[1];
    build->sr[2].uv[2] = uv3[2];
    build->sr[2].uv[3] = uv3[3];
    build->sr[2].n = normal3;
    build->sr[2].width = width;
    build->sr[2].height = height;
    
    vec3 vert41;
    vec3 vert42;
    vec3 vert43;
    vec3 normal4;
    vector<vec2> uv4;
    
    // back
    vert41.x = x;
    vert41.y = height;
    vert41.z = z-depth;
    vert42.x = x+(width/2);
    vert42.y = height+roofHeight;
    vert42.z = z-depth;
    vert43.x = x+width;
    vert43.y = height;
    vert43.z = z-depth;
    calculateVectorNormal(vert41, vert42, vert43, &normal4);
    generateTriangleUVPoints(&uv4, width);
    build->sr[3].v[0] = vert41;
    build->sr[3].v[1] = vert42;
    build->sr[3].v[2] = vert43;
    build->sr[3].uv[0] = uv4[0];
    build->sr[3].uv[1] = uv4[1];
    build->sr[3].uv[2] = uv4[2];
    build->sr[3].n = normal4;
    build->sr[3].width = width;
    build->sr[3].height = height;
    build->sr[3].isTriangle = true;
    
    build->hasSpecialtyRoof = true;
}

void Residental::generateRoofUVPoints(std::vector<comp308::vec2> *uv, float width){
    /* Depends on the width. */
    
    float roofHypt = sqrt(((storyH / 3) * (storyH / 3)) + (width*width));
    
    float yScale = roofHypt / width;
    
    vec2 uv0;
    vec2 uv1;
    vec2 uv2;
    vec2 uv3;
    
    uv0.x = 0.0;
    uv0.y = yScale;
    // (0.0, height)
    
    uv1.x = 1.0;
    uv1.y = yScale;
    // (1.0, height)
    
    uv2.x = 1.0;
    uv2.y = 0.0;
    // (1.0, 0.0)
    
    uv3.x = 0.0;
    uv3.y = 0.0;
    // (0.0, 0.0)
    
    (*uv).push_back(uv0);
    (*uv).push_back(uv1);
    (*uv).push_back(uv2);
    (*uv).push_back(uv3);
}

void Residental::generateTriangleUVPoints(std::vector<comp308::vec2> *uv, float width){
    vec2 uv0;
    vec2 uv1;
    vec2 uv2;
    
    // height / width
    float scaleY = (storyH / 2) / width;
    
    uv0.x = 0.5;
    uv0.y = scaleY;
    // (0.0, height)
    
    uv1.x = 1.0;
    uv1.y = 0;
    // (1.0, height)
    
    uv2.x = 0.0;
    uv2.y = 0.0;
    // (1.0, 0.0)
    
    
    (*uv).push_back(uv0);
    (*uv).push_back(uv1);
    (*uv).push_back(uv2);
}

void Residental::generateUVPoints(float height, float width, vector<vec2> *uv){
    float yScale = height / width;
    vec2 uv0;
    vec2 uv1;
    vec2 uv2;
    vec2 uv3;
    
    uv0.x = 0.0;
    uv0.y = yScale;
    // (0.0, height)
    
    uv1.x = 1.0;
    uv1.y = yScale;
    // (1.0, height)
    
    uv2.x = 1.0;
    uv2.y = 0.0;
    // (1.0, 0.0)
    
    uv3.x = 0.0;
    uv3.y = 0.0;
    // (0.0, 0.0)
    
    (*uv).push_back(uv0);
    (*uv).push_back(uv1);
    (*uv).push_back(uv2);
    (*uv).push_back(uv3);
}

void Residental::getBuildingType(int *type){
    (*type) = 0;
}

// -------- Commercial ----------------------------------------------------------------------------- //


Commercial::Commercial(std::vector<lot> lots, int provWTRange[], int provRTRange[], int provWDTRange[]){
    for(int i = 0; i < 2; i++){
        wallTRange[i] = provWTRange[i];
        roofTRange[i] = provRTRange[i];
        windowTRange[i] = provWDTRange[i];
    }
    subdivideLots(lots);
}

void Commercial::subdivideLots(std::vector<lot> bigLots){
    srand(time(NULL));
    // Generate sized lots
    for(unsigned int i = 0; i < bigLots.size(); i++){
        lot bigLot = bigLots[i];
        lot subdividedLot;
        
        // Check if we should just leave this biglot empty
        float random = rand() % 100;
        if(random < (chanceNL*100)){
            continue;
        }
        
        float lotDividerW = lotDRW * bigLot.width;
        float lotDividerD = lotDRD * bigLot.depth;
        
        float xPointer = bigLot.l[0].x + lotDividerW;
        float zPointer = bigLot.l[0].z - lotDividerD;
        
        
        // Big lot is cut in half depth
        float subLotDepth = ((bigLot.depth-lotDividerD*3)/ 2) ;
        float xMax = xPointer + bigLot.width;
        // While we still have xDistance left
        // Generate both front and back lots (0 = front, 1 = back)
        while(xPointer < xMax){
            float subLotWidth;
            if(xMax - xPointer < (minBWR*bigLot.width) + lotDividerW){
                // Not enough room for another house
                break;
            }
            if(xMax - xPointer < (maxBWR*bigLot.width)+lotDividerW){
                // Deals with the last lot
                subLotWidth = xMax - xPointer - (lotDividerW*2);
            }
            else{
                // Otherwise, create a random width that is between minWidth and maxWidth
                float mxr = maxBWR * bigLot.width;
                float mnr = minBWR * bigLot.width;
                float widthRatio = rand()%100;
                subLotWidth = (mxr - mnr) * (widthRatio/100) + mnr;
            }
            vec3 vector1;
            vector1.x = xPointer;
            vector1.y = groundY;
            vector1.z = zPointer;
            subdividedLot.l[0] = vector1;
            vec3 vector2;
            vector2.x = xPointer;
            vector2.y = groundY;
            vector2.z = zPointer-subLotDepth;
            subdividedLot.l[1] = vector2;
            vec3 vector3;
            vector3.x = xPointer+subLotWidth;
            vector3.y = groundY;
            vector3.z = zPointer-subLotDepth;
            subdividedLot.l[2] = vector3;
            vec3 vector4;
            vector4.x = xPointer+subLotWidth;
            vector4.y = groundY;
            vector4.z = zPointer;
            subdividedLot.l[3] = vector4;
            
            subdividedLot.width = subLotWidth;
            subdividedLot.depth = subLotDepth;
            
            cm_lots.push_back(subdividedLot);
            
            // Now increment xPointer
            xPointer = xPointer+subLotWidth+lotDividerW;
            
        }
        
        // Now lets deal with the lots behind
        xPointer = bigLot.l[1].x + lotDividerW;
        zPointer = zPointer - subLotDepth - lotDividerD;
        
        while(xPointer < xMax){
            float subLotWidth;
            if(xMax - xPointer < (minBWR*bigLot.width) + lotDividerW){
                // Not enough room for another house
                break;
            }
            if(xMax - xPointer < (maxBWR*bigLot.width)+lotDividerW){
                // Deals with the last lot
                subLotWidth = xMax - xPointer - (lotDividerW*2);
            }
            else{
                // Otherwise, create a random width that is between minWidth and maxWidth
                float mxr = maxBWR * bigLot.width;
                float mnr = minBWR * bigLot.width;
                float widthRatio = rand()%100;
                subLotWidth = (mxr - mnr) * (widthRatio/100) + mnr;
            }
            vec3 vector1;
            vector1.x = xPointer;
            vector1.y = groundY;
            vector1.z = zPointer;
            subdividedLot.l[0] = vector1;
            vec3 vector2;
            vector2.x = xPointer;
            vector2.y = groundY;
            vector2.z = zPointer-subLotDepth;
            subdividedLot.l[1] = vector2;
            vec3 vector3;
            vector3.x = xPointer+subLotWidth;
            vector3.y = groundY;
            vector3.z = zPointer-subLotDepth;
            subdividedLot.l[2] = vector3;
            vec3 vector4;
            vector4.x = xPointer+subLotWidth;
            vector4.y = groundY;
            vector4.z = zPointer;
            subdividedLot.l[3] = vector4;
            
            subdividedLot.width = subLotWidth;
            subdividedLot.depth = subLotDepth;
            
            cm_lots.push_back(subdividedLot);
            
            // Now increment xPointer
            xPointer = xPointer+subLotWidth+lotDividerW;
            
        }
    }
}

void Commercial::getLotSize(){
    cout << "commercial lots: " << cm_lots.size() << "getValue " << getValue() << endl;
}

void Commercial::getBuildingDimensions(float *height, float *width, float *depth, lot lot){
    int value = (rand() % 4) + 1;
    (*height) = value * storyH;
    (*width) = lot.width;
    (*depth) = lot.depth;
}

std::vector<lot> Commercial::getLots(){
    return cm_lots;
}

int Commercial::getValue(){
    return 200;
}

void Commercial::assignBuildingToLot(building building, int lotIndex){
    // Check to make sure index is in bounds
    cm_lots[lotIndex].lotBuilding = building;
}

void Commercial::calculateEdgeWindows(int *numWindowsHorozontal, float *windowWidth, float *windowHeight, float width){
    // For now. Only one window per edge
    (*numWindowsHorozontal) = 2;
    (*windowWidth) = width / 4;
    (*windowHeight) = storyH / 2;
}

void Commercial::assignTexture(building *build){
    // Wall texture
    if(wallTRange[0] != -1){
        int upperBound = wallTRange[1];
        int lowerLimit = wallTRange[0];
        int range = upperBound - lowerLimit;
        int chosenTexture = (rand() % range) + lowerLimit;
        // Commercial buildings have the same texture for front and side
        build->wallTexture = chosenTexture;
    }
    
    // Roof texture
    /*
     int upperBound = textureRange[1];
     int lowerLimit = textureRange[0];
     int chosenTexture = rand() % upperBound;
     build->wallTexture = chosenTexture;
     */
    if(roofTRange[0] != -1){
        int upperBound = roofTRange[1];
        int lowerLimit = roofTRange[0];
        int range = upperBound - lowerLimit;
        int chosenTexture = (rand() % range) + lowerLimit;
        build->roofTexture = chosenTexture;
    }
    
    if(windowTRange[0] != -1){
        int upperBound = windowTRange[1];
        int lowerLimit = windowTRange[0];
        int range = upperBound - lowerLimit;
        int chosenTexture = (rand() % range) + lowerLimit;
        build->windowTexture = chosenTexture;
    }
}

void Commercial::generateRoof(float x, float z, float width, float height, float depth, building *build){
    vec3 vert1;
    vec3 vert2;
    vec3 vert3;
    vec3 vert4;
    vec3 normal;
    vert1.x = x;
    vert1.y = height;
    vert1.z = z;
    vert2.x = x;
    vert2.y = height;
    vert2.z = z-depth;
    vert3.x = x+width;
    vert3.y = height;
    vert3.z = z-depth;
    vert4.x = x+width;
    vert4.y = height;
    vert4.z = z;
    calculateVectorNormal(vert1, vert2, vert3, &normal);
    build->r.v[0] = vert1;
    build->r.v[1] = vert2;
    build->r.v[2] = vert3;
    build->r.v[3] = vert4;
    build->r.n = normal;
    build->r.depth = depth;
    build->r.height = height;
    build->r.width = width;
}

void Commercial::generateUVPoints(float height, float width, vector<vec2> *uv){
    float yScale = height / width;
    vec2 uv0;
    vec2 uv1;
    vec2 uv2;
    vec2 uv3;
    
    uv0.x = 0.0;
    uv0.y = yScale;
    // (0.0, height)
    
    uv1.x = 1.0;
    uv1.y = yScale;
    // (1.0, height)
    
    uv2.x = 1.0;
    uv2.y = 0.0;
    // (1.0, 0.0)
    
    uv3.x = 0.0;
    uv3.y = 0.0;
    // (0.0, 0.0)
    
    (*uv).push_back(uv0);
    (*uv).push_back(uv1);
    (*uv).push_back(uv2);
    (*uv).push_back(uv3);
}

void Commercial::getBuildingType(int *type){
    (*type) = 1;
}

// -------- City Centre ----------------------------------------------------------------------------- //

CityCentre::CityCentre(std::vector<lot> lots, int provWTRange[], int provRTRange[], int provWDTRange[], vector<int> provTexStories){
    for(int i = 0; i < 2; i++){
        wallTRange[i] = provWTRange[i];
        roofTRange[i] = provRTRange[i];
        windowTRange[i] = provWDTRange[i];
    }
    
    textureStories = provTexStories;
    
    subdivideLots(lots);
}

void CityCentre::subdivideLots(std::vector<lot> bigLots){
    srand(time(NULL));
    // Generate sized lots
    for(unsigned int i = 0; i < bigLots.size(); i++){
        lot bigLot = bigLots[i];
        lot subdividedLot;

        float lotDividerD = lotDRD * bigLot.depth;
        float subLotDepth = bigLot.depth-lotDividerD*2;
        
        float subLotWidth;
        float lotDividerW;
        float random = rand()%100;
        if(random >= 50){
            subLotWidth = minBWR*bigLot.width;
            lotDividerW = (bigLot.width-subLotWidth*2) / 3;
        }
        else{
            subLotWidth = maxBWR*bigLot.width;
            lotDividerW = (bigLot.width - subLotWidth) / 2;
        }
        
        float xPointer = bigLot.l[0].x + lotDividerW;
        float zPointer = bigLot.l[0].z - lotDividerD;
        
        float xMax = xPointer + bigLot.width;

        while(xPointer < xMax){
            if(xMax - xPointer < (minBWR*bigLot.width) + lotDividerW){
                // Not enough room for another house
                break;
            }
            vec3 vector1;
            vector1.x = xPointer;
            vector1.y = groundY;
            vector1.z = zPointer;
            subdividedLot.l[0] = vector1;
            vec3 vector2;
            vector2.x = xPointer;
            vector2.y = groundY;
            vector2.z = zPointer-subLotDepth;
            subdividedLot.l[1] = vector2;
            vec3 vector3;
            vector3.x = xPointer+subLotWidth;
            vector3.y = groundY;
            vector3.z = zPointer-subLotDepth;
            subdividedLot.l[2] = vector3;
            vec3 vector4;
            vector4.x = xPointer+subLotWidth;
            vector4.y = groundY;
            vector4.z = zPointer;
            subdividedLot.l[3] = vector4;
            
            subdividedLot.width = subLotWidth;
            subdividedLot.depth = subLotDepth;
            
            cc_lots.push_back(subdividedLot);
            
            // Now increment xPointer
            xPointer = xPointer+subLotWidth+lotDividerW;
            
        }
    }

}

void CityCentre::getLotSize(){
    cout << "city centre lots: " << cc_lots.size() << "getValue " << getValue() << endl;
}

void CityCentre::getBuildingDimensions(float *height, float *width, float *depth, lot lot){
    int value = (rand() % 10) + 4;
    (*height) = value * storyH;
    (*width) = lot.width;
    (*depth) = lot.depth;
}

std::vector<lot> CityCentre::getLots(){
    return cc_lots;
}

void CityCentre::assignBuildingToLot(building building, int lotIndex){
    // Check to make sure index is in bounds
    cc_lots[lotIndex].lotBuilding = building;
}

void CityCentre::calculateEdgeWindows(int *numWindowsHorozontal, float *windowWidth, float *windowHeight, float width){
    // For now. Only one window per edge
    (*numWindowsHorozontal) = 0;
    (*windowWidth) = width / 8;
    (*windowHeight) = storyH / 2;
}

void CityCentre::assignTexture(building *build){
    // Wall texture
    int chosenTexture;
    if(wallTRange[0] != -1){
        int upperBound = wallTRange[1];
        int lowerLimit = wallTRange[0];
        int range = upperBound - lowerLimit;
        chosenTexture = (rand() % range) + lowerLimit;
        /* texture range and texture ids don't match up */
        build->wallTexture = chosenTexture;
        
    }
    
    // Roof texture
    /*
     int upperBound = textureRange[1];
     int lowerLimit = textureRange[0];
     int chosenTexture = rand() % upperBound;
     build->wallTexture = chosenTexture;
     */
    if(chosenTexture + roofTRange[0] < roofTRange[1]){
        build->roofTexture = chosenTexture + roofTRange[0];
    }
    
    if(windowTRange[0] != -1){
        build->windowTexture = windowTRange[0];
    }
}

void CityCentre::generateRoof(float x, float z, float width, float height, float depth, building *build){
    vec3 vert1;
    vec3 vert2;
    vec3 vert3;
    vec3 vert4;
    vec3 normal;
    vert1.x = x;
    vert1.y = height;
    vert1.z = z;
    vert2.x = x;
    vert2.y = height;
    vert2.z = z-depth;
    vert3.x = x+width;
    vert3.y = height;
    vert3.z = z-depth;
    vert4.x = x+width;
    vert4.y = height;
    vert4.z = z;
    calculateVectorNormal(vert1, vert2, vert3, &normal);
    build->r.v[0] = vert1;
    build->r.v[1] = vert2;
    build->r.v[2] = vert3;
    build->r.v[3] = vert4;
    build->r.n = normal;
    build->r.depth = depth;
    build->r.height = height;
    build->r.width = width;
}

void CityCentre::generateUVPoints(float height, float width, vector<vec2> *uv){
    float yScale = (height / storyH) / 6.0f;
    
    vec2 uv0;
    vec2 uv1;
    vec2 uv2;
    vec2 uv3;
   
    uv0.x = 0.0;
    uv0.y = yScale;
    // (0.0, height)
    
    uv1.x = 1.0;
    uv1.y = yScale;
    // (1.0, height)
    
    uv2.x = 1.0;
    uv2.y = 0.0;
    // (1.0, 0.0)
    
    uv3.x = 0.0;
    uv3.y = 0.0;
    // (0.0, 0.0)
    
    (*uv).push_back(uv0);
    (*uv).push_back(uv1);
    (*uv).push_back(uv2);
    (*uv).push_back(uv3);
}

void CityCentre::getBuildingType(int *type){
    (*type) = 2;
}

// -------- Suburb --------------------------------------------------------------------------------- //

int Suburb::getValue(){
    return 100;
}

void Suburb::generateBuildings(){
    srand(time(NULL));
    
    building build;
    
    float buildingWidth;
    float buildingDepth;
    float buildingHeight;
    
    vector<lot> lots = getLots();
    for(unsigned int i = 0; i < lots.size(); i++){
        lot lot = lots[i];
        assignTexture(&build);
        getBuildingDimensions(&buildingHeight, &buildingWidth, &buildingDepth, lot);
        generateBuilding(lot.l[0].x, lot.l[0].z, buildingWidth, buildingHeight, buildingDepth, &build);
        generateRoof(lot.l[0].x, lot.l[0].z, buildingWidth, buildingHeight, buildingDepth, &build);
        generateBuildingWindows(&build);
        assignBuildingToLot(build, i);
    }
}

void Suburb::generateBuilding(float x, float z, float width, float height, float depth, building *build){
    // bit of a hack!
    int stories = height / storyH;
    int type = -1;
    getBuildingType(&type);
    
    build->stories = stories;
    build->building_type = type;
    
    vec3 vert11;
    vec3 vert12;
    vec3 vert13;
    vec3 vert14;
    vec3 normal1;
    vector<vec2> uv1;
    
    // front
    vert11.x = x;
    vert11.y = groundY;
    vert11.z = z;
    vert12.x = x;
    vert12.y = height;
    vert12.z = z;
    vert13.x = x+width;
    vert13.y = height;
    vert13.z = z;
    vert14.x = x+width;
    vert14.y = groundY;
    vert14.z = z;
    calculateVectorNormal(vert11, vert12, vert13, &normal1);
    generateUVPoints(height, width, &uv1);
    build->e[0].v[0] = vert11;
    build->e[0].v[1] = vert12;
    build->e[0].v[2] = vert13;
    build->e[0].v[3] = vert14;
    build->e[0].uv[0] = uv1[0];
    build->e[0].uv[1] = uv1[1];
    build->e[0].uv[2] = uv1[2];
    build->e[0].uv[3] = uv1[3];
    build->e[0].n = normal1;
    build->e[0].width = width;
    build->e[0].height = height;
    
    vec3 vert21;
    vec3 vert22;
    vec3 vert23;
    vec3 vert24;
    vec3 normal2;
    vector<vec2> uv2;
    
    // right
    vert21.x = x+width;
    vert21.y = groundY;
    vert21.z = z;
    vert22.x = x+width;
    vert22.y = height;
    vert22.z = z;
    vert23.x = x+width;
    vert23.y = height;
    vert23.z = z-depth;
    vert24.x = x+width;
    vert24.y = groundY;
    vert24.z = z-depth;
    
    calculateVectorNormal(vert21, vert22, vert23, &normal2);
    generateUVPoints(height, width, &uv2);
    build->e[1].v[0] = vert21;
    build->e[1].v[1] = vert22;
    build->e[1].v[2] = vert23;
    build->e[1].v[3] = vert24;
    build->e[1].uv[0] = uv2[0];
    build->e[1].uv[1] = uv2[1];
    build->e[1].uv[2] = uv2[2];
    build->e[1].uv[3] = uv2[3];
    build->e[1].n = normal2;
    build->e[1].width = width;
    build->e[1].height = height;
    build->e[1].depth = depth;
    
    vec3 vert31;
    vec3 vert32;
    vec3 vert33;
    vec3 vert34;
    vec3 normal3;
    vector<vec2> uv3;
    
    // left
    vert31.x = x;
    vert31.y = groundY;
    vert31.z = z-depth;
    vert32.x = x;
    vert32.y = height;
    vert32.z = z-depth;
    vert33.x = x;
    vert33.y = height;
    vert33.z = z;
    vert34.x = x;
    vert34.y = groundY;
    vert34.z = z;
    calculateVectorNormal(vert31, vert32, vert33, &normal3);
    generateUVPoints(height, width, &uv3);
    build->e[2].v[0] = vert31;
    build->e[2].v[1] = vert32;
    build->e[2].v[2] = vert33;
    build->e[2].v[3] = vert34;
    build->e[2].uv[0] = uv3[0];
    build->e[2].uv[1] = uv3[1];
    build->e[2].uv[2] = uv3[2];
    build->e[2].uv[3] = uv3[3];
    build->e[2].n = normal3;
    build->e[2].width = width;
    build->e[2].height = height;
    build->e[2].depth = depth;
    
    vec3 vert41;
    vec3 vert42;
    vec3 vert43;
    vec3 vert44;
    vec3 normal4;
    vector<vec2> uv4;
    
    // back
    vert41.x = x+width;
    vert41.y = groundY;
    vert41.z = z-depth;
    vert42.x = x+width;
    vert42.y = height;
    vert42.z = z-depth;
    vert43.x = x;
    vert43.y = height;
    vert43.z = z-depth;
    vert44.x = x;
    vert44.y = groundY;
    vert44.z = z-depth;
    
    calculateVectorNormal(vert41, vert42, vert43, &normal4);
    generateUVPoints(height, width, &uv4);
    build->e[3].v[0] = vert41;
    build->e[3].v[1] = vert42;
    build->e[3].v[2] = vert43;
    build->e[3].v[3] = vert44;
    build->e[3].uv[0] = uv4[0];
    build->e[3].uv[1] = uv4[1];
    build->e[3].uv[2] = uv4[2];
    build->e[3].uv[3] = uv4[3];
    build->e[3].n = normal4;
    build->e[3].width = width;
    build->e[3].height = height;
    build->e[3].depth = depth;
    
    vec3 vert51;
    vec3 vert52;
    vec3 vert53;
    vec3 vert54;
    vec3 normal5;
}

void Suburb::generateUVWindow(window *window){
    vec2 uv0;
    vec2 uv1;
    vec2 uv2;
    vec2 uv3;
    
    uv0.x = 0.0;
    uv0.y = 1.0;
    // (0.0, height)
    
    uv1.x = 1.0;
    uv1.y = 1.0;
    // (1.0, height)
    
    uv2.x = 1.0;
    uv2.y = 0.0;
    // (1.0, 0.0)
    
    uv3.x = 0.0;
    uv3.y = 0.0;
    // (0.0, 0.0)
    
    window->uv[0] = uv0;
    window->uv[1] = uv1;
    window->uv[2] = uv2;
    window->uv[3] = uv3;
}

void Suburb::generateBuildingWindows(building *building){
    // ----- front edge windows -------- //
    int numWindowsHorozontal;
    float windowWidth;
    float windowHeight;
    
    building_edge frontEdge = building->e[0];
    int nWindowsVertical = building->stories;
    vector<window> frontEdgeWindows;
    
    calculateEdgeWindows(&numWindowsHorozontal, &windowWidth, &windowHeight, frontEdge.width);
    generateFrontWindows(numWindowsHorozontal, windowWidth, windowHeight, nWindowsVertical, faceEx, frontEdge, &frontEdgeWindows);
    building->e[0].windows = frontEdgeWindows;
   
    // ------ right edge windows ------ //
    building_edge rightEdge = building->e[1];
    vector<window> rightEdgeWindows;
    
    calculateEdgeWindows(&numWindowsHorozontal, &windowWidth, &windowHeight, rightEdge.depth);
    generateRightWindows(numWindowsHorozontal, windowWidth, windowHeight, nWindowsVertical, faceEx, rightEdge, &rightEdgeWindows);
    building->e[1].windows = rightEdgeWindows;
    
    // ------ left edge windows ------ //
    building_edge leftEdge = building->e[2];
    vector<window> leftEdgeWindows;
    
    calculateEdgeWindows(&numWindowsHorozontal, &windowWidth, &windowHeight, leftEdge.depth);
    generateLeftWindows(numWindowsHorozontal, windowWidth, windowHeight, nWindowsVertical, -faceEx, leftEdge, &leftEdgeWindows);
    building->e[2].windows = leftEdgeWindows;
    
    // ----- back edge windows -------- //
    
    building_edge backEdge = building->e[3];
    vector<window> backEdgeWindows;
    
    calculateEdgeWindows(&numWindowsHorozontal, &windowWidth, &windowHeight, backEdge.width);
    generateBackWindows(numWindowsHorozontal, windowWidth, windowHeight, nWindowsVertical, -faceEx, backEdge, &backEdgeWindows);
    building->e[3].windows = backEdgeWindows;
}

void Suburb::generateFrontWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, vector<window> *windows){
    // This method needs to calculate the spacing factor between each window
    float spacingFactorWidth = (edge.width - (nHorozontal * windowWidth)) / (nHorozontal + 1);
    float spacingFactorHeight = (storyH - windowHeight) / 2;
    float edgeX = edge.v[0].x;
    float edgeY = edge.v[0].y;
    float edgeZ = edge.v[0].z;
    float x;
    float y;
    float z = edgeZ + facingExtension;

    for (int j = 0; j < nVertical; j++){
        y = edgeY + (j * windowHeight) + ((j*2) * spacingFactorHeight ) + spacingFactorHeight;
        for (int k = 0; k < nHorozontal; k++){
            x = edgeX + (k * windowWidth) + ((k+1) * spacingFactorWidth);
            window window;
            vec3 normal;
            // for each window, need to construct 4 vectors
            vec3 vector1;
            vector1.x = x;
            vector1.y = y;
            vector1.z = z;
            window.w[0] = vector1;
            
            vec3 vector2;
            vector2.x = x;
            vector2.y = y+windowHeight;
            vector2.z = z;
            window.w[1] = vector2;
            
            vec3 vector3;
            vector3.x = x+windowWidth;
            vector3.y = y+windowHeight;
            vector3.z = z;
            window.w[2] = vector3;
            
            vec3 vector4;
            vector4.x = x+windowWidth;
            vector4.y = y;
            vector4.z = z;
            window.w[3] = vector4;
            
            generateUVWindow(&window);
            calculateVectorNormal(vector1, vector2, vector3, &normal);
            window.n = normal;
            windows->push_back(window);
            
        }
    }
}

void Suburb::generateRightWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, vector<window> *windows){
    // This method needs to calculate the spacing factor between each window
    float spacingFactorDepth = (edge.depth - (nHorozontal * windowWidth)) / (nHorozontal + 1);
    float spacingFactorHeight = (storyH - windowHeight) / 2;
    float edgeX = edge.v[0].x;
    float edgeY = edge.v[0].y;
    float edgeZ = edge.v[0].z;
    float x = edgeX + facingExtension;
    float y;
    float z;
    
    for (int j = 0; j < nVertical; j++){
        y = edgeY + (j * windowHeight) + ((j*2) * spacingFactorHeight ) + spacingFactorHeight;
        for (int k = 0; k < nHorozontal; k++){
            z = edgeZ - (k * windowWidth) - ((k+1) * spacingFactorDepth);
            window window;
            vec3 normal;
            // for each window, need to construct 4 vectors
            vec3 vector1;
            vector1.x = x;
            vector1.y = y;
            vector1.z = z;
            window.w[0] = vector1;
            
            vec3 vector2;
            vector2.x = x;
            vector2.y = y+windowHeight;
            vector2.z = z;
            window.w[1] = vector2;
            
            vec3 vector3;
            vector3.x = x;
            vector3.y = y+windowHeight;
            vector3.z = z-windowWidth;
            window.w[2] = vector3;
            
            vec3 vector4;
            vector4.x = x;
            vector4.y = y;
            vector4.z = z-windowWidth;
            window.w[3] = vector4;
            
            generateUVWindow(&window);
            calculateVectorNormal(vector1, vector2, vector3, &normal);
            window.n = normal;
            windows->push_back(window);
        }
    }
}

void Suburb::generateLeftWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, vector<window> *windows){
    // This method needs to calculate the spacing factor between each window
    float spacingFactorDepth = (edge.depth - (nHorozontal * windowWidth)) / (nHorozontal + 1);
    float spacingFactorHeight = (storyH - windowHeight) / 2;
    float edgeX = edge.v[0].x;
    float edgeY = edge.v[0].y;
    float edgeZ = edge.v[0].z;
    float x = edgeX + facingExtension;
    float y;
    float z;
    
    for (int j = 0; j < nVertical; j++){
        y = edgeY + (j * windowHeight) + ((j*2) * spacingFactorHeight ) + spacingFactorHeight;
        for (int k = 0; k < nHorozontal; k++){
            z = edgeZ + (k * windowWidth) + ((k+1) * spacingFactorDepth);
            window window;
            vec3 normal;
            // for each window, need to construct 4 vectors
            vec3 vector1;
            vector1.x = x;
            vector1.y = y;
            vector1.z = z;
            window.w[0] = vector1;
            
            vec3 vector2;
            vector2.x = x;
            vector2.y = y+windowHeight;
            vector2.z = z;
            window.w[1] = vector2;
            
            vec3 vector3;
            vector3.x = x;
            vector3.y = y+windowHeight;
            vector3.z = z+windowWidth;
            window.w[2] = vector3;
            
            vec3 vector4;
            vector4.x = x;
            vector4.y = y;
            vector4.z = z+windowWidth;
            window.w[3] = vector4;
            
            generateUVWindow(&window);
            calculateVectorNormal(vector1, vector2, vector3, &normal);
            window.n = normal;
            windows->push_back(window);
        }
    }
}

void Suburb::generateBackWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, vector<window> *windows){
    // This method needs to calculate the spacing factor between each window
    float spacingFactorWidth = (edge.width - (nHorozontal * windowWidth)) / (nHorozontal + 1);
    float spacingFactorHeight = (storyH - windowHeight) / 2;
    float edgeX = edge.v[0].x;
    float edgeY = edge.v[0].y;
    float edgeZ = edge.v[0].z;
    float x;
    float y;
    float z = edgeZ + facingExtension;
    
    for (int j = 0; j < nVertical; j++){
        y = edgeY + (j * windowHeight) + ((j*2) * spacingFactorHeight ) + spacingFactorHeight;
        for (int k = 0; k < nHorozontal; k++){
            x = edgeX - (k * windowWidth) - ((k+1) * spacingFactorWidth);
            window window;
            vec3 normal;
            // for each window, need to construct 4 vectors
            vec3 vector1;
            vector1.x = x;
            vector1.y = y;
            vector1.z = z;
            window.w[0] = vector1;
            
            vec3 vector2;
            vector2.x = x;
            vector2.y = y+windowHeight;
            vector2.z = z;
            window.w[1] = vector2;
            
            vec3 vector3;
            vector3.x = x-windowWidth;
            vector3.y = y+windowHeight;
            vector3.z = z;
            window.w[2] = vector3;
            
            vec3 vector4;
            vector4.x = x-windowWidth;
            vector4.y = y;
            vector4.z = z;
            window.w[3] = vector4;
            
            generateUVWindow(&window);
            calculateVectorNormal(vector1, vector2, vector3, &normal);
            window.n = normal;
            windows->push_back(window);
        }
    }
}


void Suburb::calculateVectorNormal(vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 *normal){
    float Qx, Qy, Qz, Px, Py, Pz;
    Qx = vertex2.x - vertex1.x;
    Qy = vertex2.y - vertex1.y;
    Qz = vertex2.z - vertex1.z;
    Px = vertex3.x - vertex1.x;
    Py = vertex3.y - vertex1.y;
    Pz = vertex3.z - vertex1.z;
    normal->x = Py*Qz - Pz*Qy;
    normal->y = Pz*Qx - Px*Qz;
    normal->z = Px*Qy - Py*Qx;
}
