//---------------------------------------------------------------------------
//
// Copyright (c) 2015 Taehyun Rhee, Joshua Scott, Ben Allen, Michael Millward, Kalo Pilato and Tim Williams
//
// The contents of this file may not be copied or duplicated in any form
// without the prior permission of its owner.
//
//----------------------------------------------------------------------------

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

#include "comp308.hpp"
#include "city.hpp"
#include "imageLoader.hpp"
#include "rainSystem.hpp"
#include "snowSystem.hpp"
#include "atmospheric.hpp"
#include "wind.hpp"
#include "shaderLoader.hpp"
#include "shadow.hpp"


using namespace std;
using namespace comp308;


// Global variables
//
GLuint g_winWidth = 1000;
GLuint g_winHeight = 800;
GLuint g_mainWindow = 0;

float g_yRotation = 0;
float g_xRotation = 0;
float g_zoomFactor = 1.0;

// Projection values
//
float g_fovy = 20.0; // 20.0
float g_znear = 0.1;
float g_zfar = 200.0;

float camYangle = 0.0f;

bool g_mouseDown = false;
vec2 g_mousePos;

// Geometry loader and drawer
//
City *city = nullptr;

// Shadow Map pointer
Shadow *shadow = nullptr;

// Weather Systems
RainSystem *rainSys = nullptr;
SnowSystem *snowSys = nullptr;
Atmospheric *atmos = nullptr;
Wind *wind = nullptr;

// Environment variables
float sky_height = 20.0f;
float startX = -20;
float startZ = -20;
float endX = 20;
float endZ = 0;
int particleCount = 10000;
int particleFrames = 60;
vec3 windVector = vec3(0.0, 0.0, 0.0);

bool raining = false;
bool snowing = false;
bool paused = false;
bool foggy = true;
bool windy = false;

bool shadowOn = true;

int textureID = 0;
 

// Navigation variables
vec3 camPosition(29.0f, 18.0f, 30.0f);
vec3 camDirection(-0.2f, -0.2f, -1.0f);
vec3 camUp(0.0f, 1.0f, 0.0f);
float camSpeed = 1.0f;

// Sets up where and what the light is
// Called once on start up
//
void initLight() {
	float direction[] = { 0.0f, 20.0f, -20.0f, 1.0f };
	float diffintensity[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	float ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };

	glLightfv(GL_LIGHT0, GL_POSITION, direction);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffintensity);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);

	glEnable(GL_LIGHT0);
}


// Sets up where the camera is in the scene
// Called every frame
//
void setUpCamera() {
	glEnable(GL_NORMALIZE);
	// Set up the projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(g_fovy, float(g_winWidth) / float(g_winHeight), g_znear, g_zfar);

	// Set up the view part of the model view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	normalize(camDirection);
	vec3 dirPos = camPosition + camDirection;
	gluLookAt(camPosition.x, camPosition.y, camPosition.z, // position of camera
		dirPos.x, dirPos.y, dirPos.z, // position to look at
		camUp.x, camUp.y, camUp.z);// up relative to camera

	glDisable(GL_NORMALIZE);
}

bool firstFrame = true;

// Draw function
//
void draw() {

	// Set up camera every frame
	setUpCamera();

	// Enable flags for normal rendering
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	atmos->clearSkyColor();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	if(shadowOn) shadow->renderShadow();
	
    // Hack to avoid drawing shadows
    if(firstFrame) {
        firstFrame = false;
        shadowOn = false;
    }
    
	city->renderCity();
    
    atmos->render();
    
    // Render Weather System
    rainSys->render(wind->wind(), camDirection);
    snowSys->render(wind->wind(), camDirection);
    
    // Disable flags for cleanup (optional)
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_NORMALIZE);
    glDisable(GL_COLOR_MATERIAL);
    
    // Move the buffer we just drew to the front
    glutSwapBuffers();
    
    // Queue the next frame to be drawn straight away
    glutPostRedisplay();
}


// Reshape function
//
void reshape(int w, int h) {
	if (h == 0) h = 1;

	g_winWidth = w;
	g_winHeight = h;
    shadow->setWindowDimensions(w, h);
    
	// Sets the openGL rendering window to match the window size
	glViewport(0, 0, g_winWidth, g_winHeight);
}


mat3 rotationMatrix(float angle, vec3 axis) {
	float x = axis.x;
	float y = axis.y;
	float z = axis.z;
	float c = cos(angle);
	float s = sin(angle);
	float t = 1.0 - c;
	return mat3(t * x * x + c, t * x * y - s * z, t * x * z + s * y,
		t * x * y + s * z, t * y * y + c, t * y * z - s * x,
		t * x * z - s * y, t * y * z + s * x, t * z * z + c);
}


// Keyboard callback
// Called once per button state change
//
void keyboardCallback(unsigned char key, int x, int y) {
	cout << "Keyboard Callback :: key=" << key << ", x,y=(" << x << "," << y << ")" << endl;
	switch (key) {
		// Camera Zoom
	case 'z':
		cout << " case 8 " << endl;
		g_zoomFactor /= 1.1;
		break;
	case 'x':
		g_zoomFactor *= 1.1;
		break;
    case '0':
        glutFullScreen();
        break;
            
		// Weather System Controls
	case '1':                       // Toggle rain on/off
		if (raining) {
			rainSys->stopRain();
            atmos->resetAtmospheric();
		}
		else {
			rainSys->startRain();
            atmos->setAtmospheric(0.015f);
		}
		raining = !raining;
		break;
	case '2':                       // Toggle snow on/off
        if (snowing){
            snowSys->stopSnow();
            atmos->resetAtmospheric();
        }
        else{
            snowSys->startSnow();
            atmos->setAtmospheric(0.015f);
        }
		snowing = !snowing;
		break;
	case '3':                       // Toggle wind on/off
		if (windy) {
			windVector = vec3(0.0, 0.0, 0.0);
		}
		else {
			windVector = vec3(0.1, 0.0, 0.0);
		}
        wind->setWind(windVector);
		windy = !windy;
	case '>':                       // Increase intensity of weather particles
		if (raining) {
			rainSys->increaseDensity();
            atmos->incrementAtmospheric();
		}
		else if (snowing) {
            snowSys->increaseDensity();
            atmos->incrementAtmospheric();
		}
		break;
	case '<':                       // Decrease intensity of weather particles
		if (raining) {
			rainSys->decreaseDensity();
            atmos->decrementAtmospheric();
		}
		else if (snowing) {
            snowSys->decreaseDensity();
            atmos->decrementAtmospheric();
		}
		break;
    case 'n':
        snowSys->toggleNoise();
        rainSys->toggleNoise();
        break;
	case 'p':
		if (paused) {
			rainSys->resumeRain();
            snowSys->resumeSnow();
		}
		else {
			rainSys->pauseRain();
            snowSys->pauseSnow();
		}
		paused = !paused;
		break;

	case 'f':
		if (foggy) {
			glDisable(GL_FOG);
		}
		else {
			glEnable(GL_FOG);
		}
		foggy = !foggy;
		break;

		// Navigation
	case 'a':
	{   vec3 camNormal = cross(camUp, camDirection);
	camPosition += camNormal * (camSpeed / 3.0);
	break;
	}
	case 'w':
		camPosition += camDirection * camSpeed;
		break;
	case 'd':
	{   vec3 camNormal = cross(camUp, camDirection);
	camPosition -= camNormal * (camSpeed / 3.0);
	break;
	}
	case 's':
		camPosition -= camDirection * camSpeed;
		break;

	case 'q':
		glutDestroyWindow(g_mainWindow);
		exit(0);

	case '*':
		if (!shadowOn)	shadowOn = true;
		else shadowOn = false;
		break;
	}
}


// Special Keyboard callback
// Called once per button state change
//
void specialCallback(int key, int x, int y) {
	cout << "Special Callback :: key=" << key << ", x,y=(" << x << "," << y << ")" << endl;
	// Not needed for this assignment, but useful to have later on
    switch(key){
        case 100:
            if(windVector.x > -0.2f) {
                windVector += vec3(-0.05f, 0.0f, 0.0f);
                wind->setWind(windVector);
            }
            break;
        case 101:
            if(windVector.z > -0.2f){
                windVector += vec3(0.0f, 0.0f, -0.05f);
                wind->setWind(windVector);
            }
            break;
        case 102:
            if(windVector.x < 0.2f){
                windVector += vec3(0.05f, 0.0f, 0.0f);
                wind->setWind(windVector);
            }
            break;
        case 103:
            if(windVector.z < 0.2f){
                windVector += vec3(0.0f, 0.0f, 0.05f);
                wind->setWind(windVector);
            }
            break;
    }
}

// Mouse Button Callback function
// Called once per button state change
//
void mouseCallback(int button, int state, int x, int y) {
	//    cout << "Mouse Callback :: button=" << button << ", state=" << state << ", (" << x << "," << y << ")" << endl;
	switch (button) {

	case 0: //left mouse button
			//cout << "case 0" << endl;
		g_mouseDown = (state == 0);
		g_mousePos = vec2(x, y);
		break;

	case 3: //scroll foward/up
			//cout << "case 3" << endl;
		g_zoomFactor /= 1.1;
		break;

	case 4: //scroll back/down
			//cout << "case 4" << endl;
		g_zoomFactor *= 1.1;
		break;
	}
}


// Mouse Motion Callback function
// Called once per frame if the mouse has moved and
// at least one mouse button has an active state
//
void mouseMotionCallback(int x, int y) {

    //    cout << "Mouse Motion Callback :: (" << x << "," << y << ")" << endl;
    if (g_mouseDown) {
		vec2 dif = vec2(x,y) - g_mousePos;
        g_mousePos = vec2(x,y);
        g_yRotation = 0.3 * dif.x;
        camYangle += g_yRotation;
        camDirection = camDirection * rotationMatrix((g_yRotation / 180.0), camUp);
        g_xRotation = 0.3 * dif.y;
        vec3 camNormal = cross(camUp, camDirection);
        camDirection = camDirection * rotationMatrix(-(g_xRotation / 180.0), camNormal);
    }
}

void loadTextures(vector<GLuint> *wallTextures, string filename) {
	GLuint texture = 0;
	image tex(filename);
	glActiveTexture(GL_TEXTURE0); // Use slot 0, need to use GL_TEXTURE1 ... etc if using more than one texture PER OBJECT
	glGenTextures(1, &texture); // Generate texture ID
	glBindTexture(GL_TEXTURE_2D, texture); // Bind it as a 2D texture

										   // Setup sampling strategies
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Finnaly, actually fill the data into our texture
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, tex.w, tex.h, tex.glFormat(), GL_UNSIGNED_BYTE, tex.dataPointer());

	// Push new texture onto wallTexture
	wallTextures->push_back(texture);

	textureID++;
}


//Main program
// 
int main(int argc, char **argv) {
	/*
	if(argc != 2){
	cout << "Obj filename expected, eg:" << endl << "    ./a1 teapot.obj" << endl;
	exit(EXIT_FAILURE);
	}*/

	// Initialise GL, GLU and GLUT
	glutInit(&argc, argv);

	// Setting up the display
	// - RGB color model + Alpha Channel = GLUT_RGBA
	// - Double buffered = GLUT_DOUBLE
	// - Depth buffer = GLUT_DEPTH
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

	// Initialise window size and create window
	glutInitWindowSize(g_winWidth, g_winHeight);
	g_mainWindow = glutCreateWindow("City Explorer");


	// Initilise GLEW
	// must be done after creating GL context (glutCreateWindow in this case)
	GLenum err = glewInit();
	if (GLEW_OK != err) { // Problem: glewInit failed, something is seriously wrong.
		cerr << "Error: " << glewGetErrorString(err) << endl;
		abort(); // Unrecoverable error
	}

	cout << "Using OpenGL " << glGetString(GL_VERSION) << endl;
	cout << "Using GLEW " << glewGetString(GLEW_VERSION) << endl;

	// Register functions for callback
	glutDisplayFunc(draw);
	glutReshapeFunc(reshape);

	glutKeyboardFunc(keyboardCallback);
	glutSpecialFunc(specialCallback);

	glutMouseFunc(mouseCallback);
	glutMotionFunc(mouseMotionCallback);

	// Create a light on the camera
	initLight();

	// Seed random
	srand(time(NULL));

	// load in textures
	vector<GLuint> residentalTextures;
	vector<GLuint> commercialTextures;
	vector<GLuint> cityCentreTextures;
	vector<GLuint> cityTextures;

	// residental walls //
	loadTextures(&residentalTextures, "work/res/assets/residental_wall_1.jpg");
	loadTextures(&residentalTextures, "work/res/assets/residental_wall_2.jpg");
	loadTextures(&residentalTextures, "work/res/assets/residental_wall_3.jpg");
	loadTextures(&residentalTextures, "work/res/assets/residental_wall_4.jpg");
	// residental window //
	loadTextures(&residentalTextures, "work/res/assets/residental_window_1.png");
	// residental roof //
	loadTextures(&residentalTextures, "work/res/assets/residental_roof_1.jpg");
	loadTextures(&residentalTextures, "work/res/assets/residental_roof_2.jpg");

	// commercial walls //
	loadTextures(&commercialTextures, "work/res/assets/commercial_wall_1.jpg");
	loadTextures(&commercialTextures, "work/res/assets/commercial_wall_2.jpg");
	// commercial windows //
	loadTextures(&commercialTextures, "work/res/assets/commercial_window_1.png");

	// city centre walls //
	loadTextures(&cityCentreTextures, "work/res/assets/city_centre_wall_1.png");
	loadTextures(&cityCentreTextures, "work/res/assets/city_centre_wall_2.png");
	loadTextures(&cityCentreTextures, "work/res/assets/city_centre_wall_3.png");
	loadTextures(&cityCentreTextures, "work/res/assets/city_centre_wall_4.png");
	// city centre roofs //
	loadTextures(&cityCentreTextures, "work/res/assets/city_centre_roof_1.png");
	loadTextures(&cityCentreTextures, "work/res/assets/city_centre_roof_2.png");

	// grass texture //
	loadTextures(&cityTextures, "work/res/assets/grass.jpg");
	loadTextures(&cityTextures, "work/res/assets/road_vertical.jpg");
	loadTextures(&cityTextures, "work/res/assets/road_horozontal.jpg");

	// Finally create our geometry
	city = new City(residentalTextures, commercialTextures, cityCentreTextures, cityTextures);

	// Add Weather System
	rainSys = new RainSystem();
	snowSys = new SnowSystem();
	atmos = new Atmospheric();
	wind = new Wind();

	shadow = new Shadow(city->getFaces(), g_winWidth, g_winHeight);

	// Sky Blue Background
	//   glClearColor(backgroundR, backgroundG, backgroundB, 1.0f);
	
	// Loop required by GLUT
	// This will not return until we tell GLUT to finish
	glutMainLoop();

	delete city;
	rainSys->clear();
	delete rainSys;
	snowSys->clear();
	delete snowSys;
	delete atmos;
	delete wind;
	delete shadow;
	
	return 0;
}