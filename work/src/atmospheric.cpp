//
//  atmospheric.cpp
//  COMP308_a4
//
//  Created by Kalo Pilato on 14/10/15.
//
//

#include "atmospheric.hpp"

Atmospheric::Atmospheric(){
    initialise();
}

void Atmospheric::initialise(){
    GLfloat fogColors[] = {fogColor, fogColor, fogColor, 1};
    glFogfv(GL_FOG_COLOR, fogColors);
    glFogi(GL_FOG_MODE, GL_EXP2);
    glFogi(GL_FOG_DENSITY, fogMinDensity);
    glEnable(GL_FOG);
}

void Atmospheric::render(){
    if(currentFrame != fogFrames){
        currentFogDensity += fogIncrement;
        currentR += rInc;
        currentG += gInc;
        currentB += bInc;
        currentFrame++;
    }
    glFogf(GL_FOG_DENSITY, currentFogDensity);
}

void Atmospheric::setAtmospheric(float density){
    targetFogDensity = density;
    fogIncrement = (targetFogDensity - currentFogDensity) / (float) fogFrames;
    rInc = (fogBackgroundR - currentR) / (float) fogFrames;
    gInc = (fogBackgroundG - currentG) / (float) fogFrames;
    bInc = (fogBackgroundB - currentB) / (float) fogFrames;
    currentFrame = 0;
}

void Atmospheric::resetAtmospheric(){
    fogFrames = 300;
    fogIncrement = 0.0f;
    fogBackgroundR = 0.353f;
    fogBackgroundG = 0.6f;
    fogBackgroundB = 0.678f;
    setAtmospheric(fogMinDensity);
}

void Atmospheric::incrementAtmospheric(){
    fogFrames = 60;
    if(targetFogDensity < fogMaxDensity){
        fogBackgroundR -= (fogBackgroundR - 0.5f) * 0.3f;
        fogBackgroundG -= (fogBackgroundG - 0.5f) * 0.3f;
        fogBackgroundB -= (fogBackgroundB - 0.5f) * 0.3f;
        setAtmospheric(targetFogDensity + 0.004);
    }
}

void Atmospheric::decrementAtmospheric(){
    fogFrames = 60;
    if(targetFogDensity > fogMinDensity){
        fogBackgroundR += (backgroundR - fogBackgroundR) * 0.3f;
        fogBackgroundG += (backgroundG - fogBackgroundG) * 0.3f;
        fogBackgroundB += (backgroundB - fogBackgroundB) * 0.3f;
        setAtmospheric(targetFogDensity - 0.004);
    }
}

void Atmospheric::clearSkyColor(){
    glClearColor(currentR, currentG, currentB, 1.0f);
}