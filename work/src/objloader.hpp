#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <glm/glm.hpp>
#include "comp308.hpp"
using namespace glm;

bool loadOBJ(
	std::vector<comp::vec3> & out_vertices,
	std::vector<comp308::vec2> & out_uvs,
	std::vector<comp308::vec3> & out_normals
);



bool loadAssImp(
	std::vector<unsigned short> & indices,
	std::vector<comp308::vec3> & vertices,
	std::vector<comp308::vec2> & uvs,
	std::vector<comp308::vec3> & normals
);

#endif