#ifndef VBOINDEXER_HPP
#define VBOINDEXER_HPP

#include <glm/glm.hpp>
#include "comp308.hpp"
using namespace glm;

void indexVBO(
	std::vector<comp308::vec3> & in_vertices,
	std::vector<comp308::vec2> & in_uvs,
	std::vector<comp308::vec3> & in_normals,

	std::vector<unsigned short> & out_indices,
	std::vector<comp308::vec3> & out_vertices,
	std::vector<comp308::vec2> & out_uvs,
	std::vector<comp308::vec3> & out_normals
);


void indexVBO_TBN(
	std::vector<comp308::vec3> & in_vertices,
	std::vector<comp308::vec2> & in_uvs,
	std::vector<comp308::vec3> & in_normals,
	std::vector<comp308::vec3> & in_tangents,
	std::vector<comp308::vec3> & in_bitangents,

	std::vector<unsigned short> & out_indices,
	std::vector<comp308::vec3> & out_vertices,
	std::vector<comp308::vec2> & out_uvs,
	std::vector<comp308::vec3> & out_normals,
	std::vector<comp308::vec3> & out_tangents,
	std::vector<comp308::vec3> & out_bitangents
);

#endif