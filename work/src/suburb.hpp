#pragma once

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "comp308.hpp"
#include "city.hpp"



// ---------- Abstract suburb class --------//
class Suburb
{
protected:
    
public:
    /* Testing */
    int getValue();
    
    /* Methods */
    void generateBuildings();
    void generateBuilding(float x, float z, float width, float stories, float depth, building *build);
    void generateBuildingWindows(building *building);
    void calculateVectorNormal(comp308::vec3 vertex1, comp308::vec3 vertex2, comp308::vec3 vertex3, comp308::vec3 *normal);
    void generateEdgeWindows(float width, float height, float depth, int nHorozontal, int nVertical, float facingExtension, building_edge edge, std::vector<window> *windows);
    
    void generateFrontWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, std::vector<window> *windows);
    void generateRightWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, std::vector<window> *windows);
    void generateLeftWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, std::vector<window> *windows);
    void generateBackWindows(int nHorozontal, float windowWidth, float windowHeight, int nVertical, float facingExtension, building_edge edge, std::vector<window> *windows);
    void generateUVWindow(window *window);
    
    /* Virtual methods */
    virtual void assignTexture(building *build) = 0;
    virtual void getLotSize() = 0;
    virtual void getBuildingDimensions(float *height, float *width, float *depth, lot lot) = 0;
    virtual std::vector<lot> getLots() = 0;
    virtual void assignBuildingToLot(building build, int lotIndex) = 0;
    virtual void calculateEdgeWindows(int *numWindowsHorozontal, float *windowWidth, float *windowHeight, float width) = 0;
    virtual void generateRoof(float x, float z, float width, float height, float depth, building *build) = 0;
    virtual void subdivideLots(std::vector<lot> bigLots) = 0;
    virtual void generateUVPoints(float height, float width, std::vector<comp308::vec2> *uv) = 0;
    virtual void getBuildingType(int *type) = 0;

};




// --------- Residental suburb class -------//
class Residental: public Suburb
{
private:
    std::vector<lot> r_lots;
    int wallTRange[2];
    int roofTRange[2];
    int windowTRange[2];
    float minBWR = 0.2;
    float maxBWR = 0.3;
    float lotDRW = 0.05;
    float lotDRD = 0.10;
    float chanceNL = 0.20;
    float chanceNW = 0.10;
public:
    Residental(std::vector<lot> lots, int textureRange[], int provRTRange[], int provWDTRange[]);
    /* Methods overidden from interface */
    void getLotSize();
    void getBuildingDimensions(float *height, float *width, float *depth, lot lot);
    std::vector<lot> getLots();
    void assignBuildingToLot(building build, int lotIndex);
    void calculateEdgeWindows(int *numWindowsHorozontal, float *windowWidth, float *windowHeight, float width);
    void assignTexture(building *build);
    void subdivideLots(std::vector<lot> bigLots);
    void generateUVPoints(float height, float width, std::vector<comp308::vec2> *uv);
    void getBuildingType(int *type);
    
    /* Unique Methods */
    void generateRoof(float x, float z, float width, float height, float depth, building *build);
    void generateTriangleUVPoints(std::vector<comp308::vec2> *uv, float width);
    void generateRoofUVPoints(std::vector<comp308::vec2> *uv, float width);
};




// --------- Commercial suburb class --------//
class Commercial: public Suburb
{
private:
    std::vector<lot> cm_lots;
    int wallTRange[2];
    int roofTRange[2];
    int windowTRange[2];
    float minBWR = 0.4;
    float maxBWR = 0.5;
    float lotDRW = 0.04;
    float lotDRD = 0.10;
    float chanceNL = 0.10;
public:
    Commercial(std::vector<lot> lots, int provWTRange[], int provRTRange[], int provWDTRange[]);
    /* Methods overidden from interface */
    void getLotSize();
    void getBuildingDimensions(float *height, float *width, float *depth, lot lot);
    std::vector<lot> getLots();
    int getValue();
    void assignBuildingToLot(building build, int lotIndex);
    void calculateEdgeWindows(int *numWindowsHorozontal, float *windowWidth, float *windowHeight, float width);
    void assignTexture(building *build);
    void subdivideLots(std::vector<lot> bigLots);
    void generateRoof(float x, float z, float width, float height, float depth, building *build);
    void generateUVPoints(float height, float width, std::vector<comp308::vec2> *uv);
    void getBuildingType(int *type);
};



// --------- CityCentre suburb class --------//
class CityCentre: public Suburb {
private:
    std::vector<lot> cc_lots;
    int wallTRange[2];
    int roofTRange[2];
    int windowTRange[2];
    std::vector<int> textureStories;
    float minBWR = 0.35;
    float maxBWR = 0.50;
    //float lotDRW = 0.04; <- Depends on minBWR and maxBWR
    float lotDRD = 0.10;
public:
    CityCentre(std::vector<lot> lots, int provWTRange[], int provRTRange[], int provWDTRange[], std::vector<int> textureStories);
    /* Methods overidden from interface */
    void getLotSize();
    void getBuildingDimensions(float *height, float *width, float *depth, lot lot);
    std::vector<lot> getLots();
    void assignBuildingToLot(building build, int lotIndex);
    void calculateEdgeWindows(int *numWindowsHorozontal, float *windowWidth, float *windowHeight, float width);
    void assignTexture(building *build);
    void subdivideLots(std::vector<lot> bigLots);
    void generateRoof(float x, float z, float width, float height, float depth, building *build);
    void generateUVPoints(float height, float width, std::vector<comp308::vec2> *uv);
    void getBuildingType(int *type);
};

