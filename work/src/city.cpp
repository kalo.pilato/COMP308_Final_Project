//---------------------------------------------------------------------------
//
// Copyright (c) 2015 Michael Millward, Kalo Pilato and Tim Williams
//
//----------------------------------------------------------------------------

#include <cmath>
#include <iostream> // input/output streams
#include <fstream>  // file streams
#include <sstream>  // string streams
#include <string>
#include <stdexcept>
#include <vector>
#include <map>

#include "comp308.hpp"
#include "city.hpp"
#include "suburb.hpp"

using namespace std;
using namespace comp308;

/* Changeable fields */
float cityWidth = 75.0f;
float cityDepth = 120.0f;
int noRoadsHorozontal = 6;
int noRoadsDeep = 10;

/* Dont recomend changing */
Residental *residental = nullptr;
Commercial *commercial = nullptr;
CityCentre *citycentre = nullptr;

vector<window> faces;

vector<GLuint> residental_textures;
vector<GLuint> commercial_textures;
vector<GLuint> city_centre_textures;
vector<GLuint> city_textures;

/* residental texture ranges */
int residental_wall_texture_range[2] = {0, 4};
int residental_window_texture_range[2] = {4, 5};
int residental_roof_texture_range[2] = {5, 7};

/* commerical texture ranges */
int commercial_wall_texture_range[2] = {0, 2};
int commerical_window_texture_range[2] = {2, 3};
int commerical_roof_texture_range[2] = {-1, -1};

/* city centre texture ranges */
int city_centre_wall_texture_range[2] = {0, 4};
int city_centre_window_texture_range[2] = {-1, -1};
int city_centre_roof_texture_range[2] = {4, 6};

float roadRatio = 0.20;
float faceExtension = 0.07;
float ground = 0.0f;

// Testing purpose
vector<lot> bigLots;
int suburbTester = 0;

City::City(std::vector<GLuint> providedResidentalTextures, std::vector<GLuint> providedCommercialTextures, std::vector<GLuint> providedCityCentreTextures, std::vector<GLuint> providedCityTextures) {
    residental_textures = providedResidentalTextures;
    commercial_textures = providedCommercialTextures;
    city_centre_textures = providedCityCentreTextures;
    city_textures = providedCityTextures;
    
    generateCityFloor();
    generateBuildings();
}

void City::generateCityFloor(){
    generated_city.cityWidth = cityWidth;
    generated_city.cityDepth = cityDepth;
    
    // Generate all corners of the city
    vec3 frontLeft;
    vec3 frontRight;
    vec3 backRight;
    vec3 backLeft;
    
    frontLeft.x = - (cityWidth / 2);
    frontLeft.y = ground;
    frontLeft.z = 0;
    
    frontRight.x = cityWidth / 2;
    frontRight.y = ground;
    frontRight.z = 0;
    
    backRight.x = cityWidth / 2;
    backRight.y = ground;
    backRight.z = - cityDepth;
    
    backLeft.x = - (cityWidth / 2);
    backLeft.y = ground;
    backLeft.z = - cityDepth;
    
    generated_city.c[0] = frontLeft;
    generated_city.c[1] = frontRight;
    generated_city.c[2] = backRight;
    generated_city.c[3] = backLeft;
    
    generateGrassUVPoints();

    /* Spacing between roads horozontally */
    float roadSpacingWidth;
    float roadSpacingDepth;
    float roadWidth;
    
    roadSpacingDepth = cityDepth / ((noRoadsDeep-1) + roadRatio*noRoadsDeep);
    roadWidth = roadSpacingDepth * roadRatio;
    roadSpacingWidth = (cityWidth - roadWidth*noRoadsHorozontal) / (noRoadsHorozontal-1);
    
    generateRoads(roadSpacingWidth, roadSpacingDepth, roadWidth, cityDepth, 'v');
    generateRoads(roadSpacingWidth, roadSpacingDepth, cityWidth, roadWidth, 'h');
    generateRoadUVPoints();
    generateLots(roadSpacingWidth, roadSpacingDepth, roadWidth);
}

void City::generateRoadUVPoints(){
    for(unsigned long i = 0; i < generated_city.roads.size(); i++){
        road road = generated_city.roads[i];
        vec2 frontLeft;
        vec2 backLeft;
        vec2 backRight;
        vec2 frontRight;
        
        float xScale = 1.0;
        float dScale = 1.0;
        
        // Find out which distance is the road width
        //cout << "in uv road orien: " << road.orientation << endl;
        
        
        frontLeft.x = 0.0;
        frontLeft.y = 0.0;
        
        backLeft.x = 0.0;
        backLeft.y = dScale;
        
        backRight.x = xScale;
        backRight.y = dScale;
        
        frontRight.x = xScale;
        frontRight.y = 0.0;
        
        generated_city.roads[i].uv[0] = frontLeft;
        generated_city.roads[i].uv[1] = backLeft;
        generated_city.roads[i].uv[2] = backRight;
        generated_city.roads[i].uv[3] = frontRight;
        
        if(road.orientation == 'v'){
            generated_city.roads[i].roadTexture = 1;
        }
        else{
            generated_city.roads[i].roadTexture = 2;
        }
    }
}

void City::generateGrassUVPoints(){
    float xScale = cityWidth / 5;
    float dScale = cityDepth / 5;
    
    vec2 frontLeft;
    vec2 backLeft;
    vec2 backRight;
    vec2 frontRight;
    
    frontLeft.x = 0.0;
    frontLeft.y = dScale;
    
    backLeft.x = xScale;
    backLeft.y = dScale;
    
    backRight.x = xScale;
    backRight.y = 0.0;
    
    frontRight.x = 0.0;
    frontRight.y = 0.0;
    
    generated_city.uv[0] = frontLeft;
    generated_city.uv[1] = backLeft;
    generated_city.uv[2] = backRight;
    generated_city.uv[3] = frontRight;
}

void City::generateLots(float roadSpacingWidth, float roadSpacingDepth, float roadWidth){
    // Generate big lots
    float cityX = generated_city.c[0].x;
    float cityY = generated_city.c[0].y;
    float cityZ = generated_city.c[0].z;
    float x;
    float y = cityY + faceExtension;
    float z = cityZ;
    lot generatedLot;
    
    
    // Find the index of the building in the city centre
    int lotCityCentreX = (noRoadsHorozontal-1) / 2;
    int lotCityCentreZ = (noRoadsDeep-1) / 2;
    
    // Calculate the max distance from the centre lot
    int lotCount = 0;
    int maxDistance = -1;
    for(int j = 0; j < (noRoadsDeep-1); j++){
        z = cityZ + (j * roadSpacingDepth) + ((j+1) * roadWidth);
        for (int k = 0; k < (noRoadsHorozontal-1); k++) {
            lotCount++;
            x = cityX + (k * roadSpacingWidth) + ((k+1) * roadWidth);
            int xPosCentr = abs(lotCityCentreX - k);
            int yPosCentr = abs(lotCityCentreZ - j);
            int distanceFromCentre = (int) sqrt(xPosCentr*xPosCentr + yPosCentr*yPosCentr);
            if(maxDistance < distanceFromCentre){
                maxDistance = distanceFromCentre;
            }
        }
    }
    
    std::vector<int> suburbRanges;
    computeSuburbRanges(maxDistance, &suburbRanges);
    
    vector<lot> residentalLots;
    vector<lot> commercialLots;
    vector<lot> citycentreLots;
    
    for (int j = 0; j < (noRoadsDeep-1); j++){
        z = cityZ - (j * roadSpacingDepth) - ((j+1) * roadWidth);
        for (int k = 0; k < (noRoadsHorozontal-1); k++){
            x = cityX + (k * roadSpacingWidth) + ((k+1) * roadWidth);
            
            vec3 vector1;
            vector1.x = x;
            vector1.y = y;
            vector1.z = z;
            generatedLot.l[0] = vector1;
        
            vec3 vector2;
            vector2.x = x;
            vector2.y = y;
            vector2.z = z-roadSpacingDepth;
            generatedLot.l[1] = vector2;
        
            vec3 vector3;
            vector3.x = x+roadSpacingWidth;
            vector3.y = y;
            vector3.z = z-roadSpacingDepth;
            generatedLot.l[2] = vector3;
        
            vec3 vector4;
            vector4.x = x+roadSpacingWidth;
            vector4.y = y;
            vector4.z = z;
            generatedLot.l[3] = vector4;
            
            generatedLot.width = roadSpacingWidth;
            generatedLot.depth = roadSpacingDepth;
            
            // calculate distance from centre
            int xPosCentr = abs(lotCityCentreX - k);
            int yPosCentr = abs(lotCityCentreZ - j);
            int distanceFromCentre = (int) sqrt(xPosCentr*xPosCentr + yPosCentr*yPosCentr);
            generatedLot.distanceFromCentre = distanceFromCentre;
            
            // Now find what suburb this lot is in
            bigLots.push_back(generatedLot);
            if(distanceFromCentre >= suburbRanges[0] && distanceFromCentre <= suburbRanges[1]){
                citycentreLots.push_back(generatedLot);
            }
            else if(distanceFromCentre >= suburbRanges[2] && distanceFromCentre <= suburbRanges[3]){
                commercialLots.push_back(generatedLot);
            }
            else if(distanceFromCentre >= suburbRanges[4] && distanceFromCentre <= suburbRanges[5]){
                residentalLots.push_back(generatedLot);
            }
            else{
                cout << "suburb ranges are incorrect " << endl;
                abort();
            }
        }
    }
    
    residental = new Residental(residentalLots, residental_wall_texture_range, residental_roof_texture_range, residental_window_texture_range);
    commercial = new Commercial(commercialLots, commercial_wall_texture_range, commerical_roof_texture_range, commerical_window_texture_range);
    
    vector<int> textStories;
    textStories.push_back(6);
    textStories.push_back(6);
   
    citycentre = new CityCentre(citycentreLots, city_centre_wall_texture_range, city_centre_roof_texture_range, city_centre_window_texture_range, textStories);
}

/* This method is in charge of determining where the suburbs are on the map. */
void City::computeSuburbRanges(int maxDistance, std::vector<int> *suburbRanges){
    // City has three areas 1 = residental, 2 = commerical, 3 = heavily residental.
    // Need to find the range of these areas
    int rangeOfAreas = maxDistance / 3;
    
    // Error checking
    if (rangeOfAreas < 1){
        // Need to something better here
        cout << "City is too small. Range is: " << rangeOfAreas << endl;
        abort();
    }
    (*suburbRanges).push_back(0);
    (*suburbRanges).push_back(rangeOfAreas);
    (*suburbRanges).push_back(rangeOfAreas+1);
    (*suburbRanges).push_back(2*rangeOfAreas);
    (*suburbRanges).push_back((2*rangeOfAreas)+1);
    (*suburbRanges).push_back(maxDistance);

}

/* horoOrVert indicates whether the roads are being drawn vertically on the ground or horozontally. 0 = vert. 1 = horo */
void City::generateRoads(float roadSpacingWidth, float roadSpacingDeep, float roadXLength, float roadZLength, char vertOrHoro){
    float xPointer = generated_city.c[0].x;
    float zPointer = generated_city.c[0].z;
    float yPointer = ground + faceExtension;
    
    if(vertOrHoro == 'h'){
        yPointer += 0.02;
    }
    
    // Construct the very first road
    vec3 roadFrontLeft;
    vec3 roadFrontRight;
    vec3 roadBackRight;
    vec3 roadBackLeft;
    road road;
    
    // Hack to prevent intial road blur begins //
    roadFrontLeft.x = 0;
    roadFrontLeft.y = -10;
    roadFrontLeft.z = 0;
    roadFrontRight.x = 0;
    roadFrontRight.y = -10;
    roadFrontRight.z = 0;
    roadBackRight.x = 0;
    roadBackRight.y = -10;
    roadBackRight.z = 0;
    roadBackLeft.x = 0;
    roadBackLeft.y = -10;
    roadBackLeft.z = 0;
    road.r[0] = roadFrontLeft;
    road.r[1] = roadFrontRight;
    road.r[2] = roadBackRight;
    road.r[3] = roadBackLeft;
    road.orientation = vertOrHoro;
    generated_city.roads.push_back(road);
    // Hack to prevent intial road blur ends //
    
    roadFrontLeft.x = xPointer;
    roadFrontLeft.y = yPointer;
    roadFrontLeft.z = zPointer;
    
    roadFrontRight.x = xPointer+roadXLength;
    roadFrontRight.y = yPointer;
    roadFrontRight.z = zPointer;
    
    roadBackRight.x = xPointer+roadXLength;
    roadBackRight.y = yPointer;
    roadBackRight.z = zPointer-roadZLength;
    
    roadBackLeft.x = xPointer;
    roadBackLeft.y = yPointer;
    roadBackLeft.z = zPointer-roadZLength;
    
    
    road.r[0] = roadFrontLeft;
    road.r[1] = roadFrontRight;
    road.r[2] = roadBackRight;
    road.r[3] = roadBackLeft;
    road.orientation = vertOrHoro;
    generated_city.roads.push_back(road);
    
    // including the first road
    int i;
    if(vertOrHoro == 'v'){
        i = noRoadsHorozontal - 1;
        xPointer = xPointer + roadXLength;
    }
    else {
        i = noRoadsDeep - 1;
        zPointer = zPointer - roadZLength;
    }
    while(i > 0){
        // each loop creates a new road
        if(vertOrHoro == 'v'){
            xPointer = xPointer + roadSpacingWidth;
        }
        else if(vertOrHoro == 'h'){
            zPointer = zPointer - roadSpacingDeep;
        }
        roadFrontLeft.x = xPointer;
        roadFrontLeft.y = yPointer;
        roadFrontLeft.z = zPointer;
        
        roadFrontRight.x = xPointer+roadXLength;
        roadFrontRight.y = yPointer;
        roadFrontRight.z = zPointer;
        
        roadBackRight.x = xPointer+roadXLength;
        roadBackRight.y = yPointer;
        roadBackRight.z = zPointer-roadZLength;
        
        roadBackLeft.x = xPointer;
        roadBackLeft.y = yPointer;
        roadBackLeft.z = zPointer-roadZLength;
        
        road.r[0] = roadFrontLeft;
        road.r[1] = roadFrontRight;
        road.r[2] = roadBackRight;
        road.r[3] = roadBackLeft;
        road.orientation = vertOrHoro;
        
        //cout << "road orien: " << road.orientation << endl;
        
        generated_city.roads.push_back(road);
        
        if(vertOrHoro == 'v'){
            xPointer = xPointer + roadXLength;
            i = i - 1;
        }
        else if(vertOrHoro == 'h'){
            zPointer = zPointer - roadZLength;
            i = i - 1;
        }
    }
}

/* The Draw function */
void City::renderCity() {
    glColor3f(0.0, 1.0, 0.0);
    // ----------------------- ground ------------------------- //
    GLuint texture = city_textures[generated_city.grassTexture];
    
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glActiveTexture(GL_TEXTURE0);
    
    glBindTexture(GL_TEXTURE_2D, texture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glBegin(GL_QUADS);
    
    for( int i = 0; i < 4; i++ ){
        glVertex3f(generated_city.c[i].x,generated_city.c[i].y, generated_city.c[i].z);
        glTexCoord2d(generated_city.uv[i].x, generated_city.uv[i].y);
    }
    
    glEnd();
    
    glActiveTextureARB(GL_TEXTURE0_ARB); // Select texture unit 0
    glBindTexture(GL_TEXTURE_2D, 0);     // unbind texture for unit 0
    glDisable(GL_TEXTURE_2D);            // disable texturing for unit 0
    
    // ----------------------- roads ------------------------- //
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glActiveTexture(GL_TEXTURE0);

    for(unsigned long i = 0; i < generated_city.roads.size(); i++){
        road road = generated_city.roads[i];
        
        GLuint texture = city_textures[road.roadTexture];
        glBindTexture(GL_TEXTURE_2D, texture);
    
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
        glBegin(GL_QUADS);
        
        glVertex3f(road.r[0].x,road.r[0].y,road.r[0].z);
        glTexCoord2d(road.uv[0].x, road.uv[0].y);
        //cout << "road uv x: " << road.uv[0].x << "road uv y: " << road.uv[0].y << endl;
        
        glVertex3f(road.r[1].x,road.r[1].y,road.r[1].z);
        glTexCoord2d(road.uv[1].x, road.uv[1].y);
        //cout << "road uv x: " << road.uv[1].x << "road uv y: " << road.uv[1].y << endl;
        
        glVertex3f(road.r[2].x,road.r[2].y,road.r[2].z);
        glTexCoord2d(road.uv[2].x, road.uv[2].y);
        //cout << "road uv x: " << road.uv[2].x << "road uv y: " << road.uv[2].y << endl;
        
        glVertex3f(road.r[3].x,road.r[3].y,road.r[3].z);
        glTexCoord2d(road.uv[3].x, road.uv[3].y);
        //cout << "road uv x: " << road.uv[3].x << "road uv y: " << road.uv[3].y << endl;
        
        glEnd();
        
    }
    glActiveTextureARB(GL_TEXTURE0_ARB); // Select texture unit 0
    glBindTexture(GL_TEXTURE_2D, 0);     // unbind texture for unit 0
    glDisable(GL_TEXTURE_2D);            // disable texturing for unit 0
    
    // ----------------------- lots ------------------------- //
    for(unsigned long i = 0; i < citycentre->getLots().size(); i++){
        glColor3f(0.6, 0.6, 0.6);
        lot l = citycentre->getLots()[i];
        glBegin(GL_POLYGON);
        for( int i = 0; i < 4; i++ ){
            glVertex3f(l.l[i].x, l.l[i].y + 0.4, l.l[i].z);
        }
        glEnd();
        
    }
    // Go through each suburb and get each lot.
    renderSuburb(residental->getLots());
    renderSuburb(commercial->getLots());
    renderSuburb(citycentre->getLots());
    
    /*
    // Test for faces
    glColor3f(0.6, 0.6, 0.6);
    for(int i = 0; i < faces.size(); i++){
        window w = faces[i];
        glBegin(GL_POLYGON);
        int range = 4;
        if(w.w[3].x == -101 && w.w[3].y == -101 && w.w[3].z == -101){
            range = 3;
        }
        for(int j = 0; j < range; j++){
            glVertex3f(w.w[j].x, w.w[j].y, w.w[j].z);
        }
        glNormal3f(w.n.x, w.n.y, w.n.z);
        glEnd();
    } */
}

GLuint City::getWallTexture(building build){
    if(build.wallTexture == -1){
        return NULL;
    }
    if(build.building_type == 0){
        // building = residental
        return residental_textures[build.wallTexture];
    }
    else if(build.building_type == 1){
        // building = commecial
        return commercial_textures[build.wallTexture];
    }
    else if(build.building_type == 2){
        // building = cityCentre
        return city_centre_textures[build.wallTexture];
    }
    return -1;
}

GLuint City::getWindowTexture(building build){
    if(build.windowTexture == -1){
        return NULL;
    }
    if(build.building_type == 0){
        return residental_textures[build.windowTexture];
    }
    else if(build.building_type == 1){
        return commercial_textures[build.windowTexture];
    }
    else if(build.building_type == 2){
        return city_centre_textures[build.windowTexture];
    }
    return -1;
}


GLuint City::getRoofTexture(building build){
    if(build.roofTexture == -1){
        return NULL;
    }
    if(build.building_type == 0){
        return residental_textures[build.roofTexture];
    }
    else if(build.building_type == 1){
        return commercial_textures[build.roofTexture];
    }
    else if(build.building_type == 2){
        return city_centre_textures[build.roofTexture];
    }
    return -1;
}

void City::renderSuburb(vector<lot> lots){
    GLfloat amTable[] = {0.4, 0.4, 0.4};
    GLfloat difTable[] = {0.7, 0.7, 0.7};
    GLfloat specTable[] = {0.30, 0.30, 0.30};
    
    // Sets the reflectivity of the matieral
    glMaterialfv(GL_FRONT, GL_AMBIENT, amTable);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, difTable);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specTable);
    glMaterialf(GL_FRONT, GL_SHININESS, 25.0); // 0 -> 128
    
    for(unsigned long i = 0; i < lots.size(); i++){
        building build = lots[i].lotBuilding;
        for(int j = 0; j < 4; j++){
            glColor3f(0.6, 0.6, 0.6);
            building_edge edge = build.e[j];
            GLuint texture = getWallTexture(build);
            if(texture != (GLuint) -1){
                glEnable(GL_TEXTURE_2D);
                glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                glActiveTexture(GL_TEXTURE0);
                    
                glBindTexture(GL_TEXTURE_2D, texture);
                    
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MODULATE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MODULATE);
                
                glBegin(GL_QUADS);
                
                glVertex3f(edge.v[0].x, edge.v[0].y, edge.v[0].z);
                glTexCoord2d(edge.uv[0].x, edge.uv[0].y);
                    
                glVertex3f(edge.v[1].x, edge.v[1].y, edge.v[1].z);
                glTexCoord2d(edge.uv[1].x, edge.uv[1].y);
                    
                glVertex3f(edge.v[2].x, edge.v[2].y, edge.v[2].z);
                glTexCoord2d(edge.uv[2].x, edge.uv[2].y);
                
                glVertex3f(edge.v[3].x, edge.v[3].y, edge.v[3].z);
                glTexCoord2d(edge.uv[3].x, edge.uv[3].y);
                
                glNormal3f(edge.n.x, edge.n.y, edge.n.z);
                
                glEnd();
                
                glActiveTextureARB(GL_TEXTURE0_ARB); // Select texture unit 0
                glBindTexture(GL_TEXTURE_2D, 0);     // unbind texture for unit 0
                glDisable(GL_TEXTURE_2D);            // disable texturing for unit 0
            }
            else{
                glColor3f(0.6, 0.6, 0.6);
                glBegin(GL_POLYGON);
                for (int k = 0; k < 4; k++) {
                    glVertex3f(edge.v[k].x, edge.v[k].y, edge.v[k].z);
                }
                glNormal3f(edge.n.x, edge.n.y, edge.n.z);
                glEnd();
            }
            for(unsigned long k = 0; k < edge.windows.size(); k++){
                window window = edge.windows[k];
                GLuint texture = getWindowTexture(build);
                if(texture != (GLuint) -1){
                    glEnable(GL_TEXTURE_2D);
                    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                    glActiveTexture(GL_TEXTURE0);
                    
                    glBindTexture(GL_TEXTURE_2D, texture);
                    
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                    
                    glBegin(GL_QUADS);
                    
                    for(int l = 0; l < 4; l++){
                        glVertex3f(window.w[l].x, window.w[l].y, window.w[l].z);
                        glTexCoord2d(window.uv[l].x, window.uv[l].y);
                    }
                    glNormal3f(window.n.x, window.n.y, window.n.z);
                    
                    glEnd();
                    
                    glActiveTextureARB(GL_TEXTURE0_ARB); // Select texture unit 0
                    glBindTexture(GL_TEXTURE_2D, 0);     // unbind texture for unit 0
                    glDisable(GL_TEXTURE_2D);            // disable texturing for unit 0
                
                }
                else{
                    glBegin(GL_QUADS);
                    for(int l = 0; l < 4; l++){
                        glVertex3f(window.w[l].x, window.w[l].y, window.w[l].z);
                    }
                    glNormal3f(window.n.x, window.n.y, window.n.z);
                    glEnd();
                }
            }
        }
        if(build.hasSpecialtyRoof){
            for(int j = 0; j < 4; j++){
                building_edge roofEdge = build.sr[j];
                if(roofEdge.isTriangle){
                    GLuint texture = getRoofTexture(build);

                    glEnable(GL_TEXTURE_2D);
                    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                    glActiveTexture(GL_TEXTURE0);
                        
                    glBindTexture(GL_TEXTURE_2D, texture);
                        
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                        
                    glBegin(GL_POLYGON);
                    
                    glVertex3f(roofEdge.v[0].x, roofEdge.v[0].y, roofEdge.v[0].z);
                    glTexCoord2d(roofEdge.uv[0].x, roofEdge.uv[0].y);
                    
                    glVertex3f(roofEdge.v[1].x, roofEdge.v[1].y, roofEdge.v[1].z);
                    glTexCoord2d(roofEdge.uv[1].x, roofEdge.uv[1].y);
                    
                    glVertex3f(roofEdge.v[2].x, roofEdge.v[2].y, roofEdge.v[2].z);
                    glTexCoord2d(roofEdge.uv[2].x, roofEdge.uv[2].y);

                    glNormal3f(roofEdge.n.x, roofEdge.n.y, roofEdge.n.z);
                    glEnd();
                        
                    glActiveTextureARB(GL_TEXTURE0_ARB); // Select texture unit 0
                    glBindTexture(GL_TEXTURE_2D, 0);     // unbind texture for unit 0
                    glDisable(GL_TEXTURE_2D);            // disable texturing for unit 0
         
                }
                else{
                    GLuint texture = getRoofTexture(build);
                    
                    glEnable(GL_TEXTURE_2D);
                    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                    glActiveTexture(GL_TEXTURE0);
                    
                    glBindTexture(GL_TEXTURE_2D, texture);
                    
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                    
                    glBegin(GL_POLYGON);
                    
                    glVertex3f(roofEdge.v[0].x, roofEdge.v[0].y, roofEdge.v[0].z);
                    glTexCoord2d(roofEdge.uv[0].x, roofEdge.uv[0].y);
                    
                    glVertex3f(roofEdge.v[1].x, roofEdge.v[1].y, roofEdge.v[1].z);
                    glTexCoord2d(roofEdge.uv[1].x, roofEdge.uv[1].y);
                    
                    glVertex3f(roofEdge.v[2].x, roofEdge.v[2].y, roofEdge.v[2].z);
                    glTexCoord2d(roofEdge.uv[2].x, roofEdge.uv[2].y);
                    
                    glVertex3f(roofEdge.v[3].x, roofEdge.v[3].y, roofEdge.v[3].z);
                    glTexCoord2d(roofEdge.uv[3].x, roofEdge.uv[3].y);
                    
                    glNormal3f(roofEdge.n.x, roofEdge.n.y, roofEdge.n.z);
                    glEnd();
                    
                    glActiveTextureARB(GL_TEXTURE0_ARB); // Select texture unit 0
                    glBindTexture(GL_TEXTURE_2D, 0);     // unbind texture for unit 0
                    glDisable(GL_TEXTURE_2D);            // disable texturing for unit 0
                }
            }
        
        }
        else {
            // doesn't have a specialty roof. just draw normal roof.
            GLuint texture = getRoofTexture(build);
            building_edge roofEdge = build.r;
            if(texture != (GLuint) -1){
                glEnable(GL_TEXTURE_2D);
                glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                glActiveTexture(GL_TEXTURE0);
                
                glBindTexture(GL_TEXTURE_2D, texture);
                
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                
                glBegin(GL_POLYGON);
                
                glVertex3f(roofEdge.v[0].x, roofEdge.v[0].y, roofEdge.v[0].z);
                glTexCoord2d(roofEdge.uv[0].x, roofEdge.uv[0].y);
                
                glVertex3f(roofEdge.v[1].x, roofEdge.v[1].y, roofEdge.v[1].z);
                glTexCoord2d(roofEdge.uv[1].x, roofEdge.uv[1].y);
                
                glVertex3f(roofEdge.v[2].x, roofEdge.v[2].y, roofEdge.v[2].z);
                glTexCoord2d(roofEdge.uv[2].x, roofEdge.uv[2].y);
                
                glVertex3f(roofEdge.v[3].x, roofEdge.v[3].y, roofEdge.v[3].z);
                glTexCoord2d(roofEdge.uv[3].x, roofEdge.uv[3].y);
                
                glNormal3f(roofEdge.n.x, roofEdge.n.y, roofEdge.n.z);
                glEnd();
                
                glActiveTextureARB(GL_TEXTURE0_ARB); // Select texture unit 0
                glBindTexture(GL_TEXTURE_2D, 0);     // unbind texture for unit 0
                glDisable(GL_TEXTURE_2D);            // disable texturing for unit 0
            }
            else{
                glBegin(GL_QUADS);
                for(int l = 0; l < 4; l++){
                    glVertex3f(roofEdge.v[l].x, roofEdge.v[l].y, roofEdge.v[l].z);
                }
                glNormal3f(roofEdge.n.x, roofEdge.n.y, roofEdge.n.z);
                glEnd();
            
            }
        }
    }
}

vector<window> City::getFaces(){
    return faces;
}

void City::constructEdges(){
    getSuburbEdges(residental->getLots(), &faces);
    getSuburbEdges(commercial->getLots(), &faces);
    getSuburbEdges(citycentre->getLots(), &faces);
    window ground;
    vec3 normal;
    ground.w[0] = generated_city.c[0];
    ground.w[1] = generated_city.c[1];
    ground.w[2] = generated_city.c[2];
    ground.w[3] = generated_city.c[3];
    calculateVectorNormal(ground.w[0], ground.w[1], ground.w[2], &normal);
    ground.n = normal;
    ground.uv[0] = generated_city.uv[0];
    ground.uv[0] = generated_city.uv[1];
    ground.uv[0] = generated_city.uv[2];
    ground.uv[0] = generated_city.uv[3];
    faces.push_back(ground);
}

void City::getSuburbEdges(vector<lot> lots, vector<window> *allWindows){
    for(unsigned long i = 0; i < lots.size(); i++){
        building build = lots[i].lotBuilding;
        
        // Windows and edges
        for(int j = 0; j < 4; j++){
            window w;
            building_edge edge = build.e[j];
            edgeToWindow(edge, &w);
            allWindows->push_back(w);
        }
        
        // Specialty roof
        if(build.hasSpecialtyRoof){
            for(int k = 0; k < 4; k++){
                building_edge roofEdge = build.sr[k];
                window win;
                
                if(roofEdge.isTriangle){
                    // Special case building edge.... when the edge has three vertices
                    for(int l = 0; l < 3; l++){
                        vec3 vertex = roofEdge.v[l];
                        win.w[l].x = vertex.x;
                        win.w[l].y = vertex.y;
                        win.w[l].z = vertex.z;
                    }
                    
                    win.w[3].x = -101;
                    win.w[3].y = -101;
                    win.w[3].z = -101;
                    
                    win.n = roofEdge.n;
                    for(int l = 0; l < 3; l++){
                        vec2 uv = roofEdge.uv[l];
                        win.uv[l].x = uv.x;
                        win.uv[l].y = uv.y;
                    }
                }
                else{
                    edgeToWindow(roofEdge, &win);
                }
                allWindows->push_back(win);
            }
        }
        
        // Normal roof
        else{
            window win;
            building_edge roofEdge = build.r;
            edgeToWindow(roofEdge, &win);
            allWindows->push_back(win);
        }
    }
}

void City::edgeToWindow(building_edge edge, window *window){
    // assign all v
    for(int i = 0; i < 4; i++){
        vec3 vertex = edge.v[i];
        window->w[i].x = vertex.x;
        window->w[i].y = vertex.y;
        window->w[i].z = vertex.z;
    }
    // asssign n
    window->n = edge.n;
    // assign all uv
    for(int i = 0; i < 4; i++){
        vec2 uv = edge.uv[i];
        window->uv[i].x = uv.x;
        window->uv[i].y = uv.y;
    }
}


/* Generates the building list of the city */
void City::generateBuildings(){
    residental->generateBuildings();
    commercial->generateBuildings();
    citycentre->generateBuildings();
    constructEdges();
}

/* Utility function to calculate vector normals of a given plane */
void City::calculateVectorNormal(vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 *normal){
    float Qx, Qy, Qz, Px, Py, Pz;
    Qx = vertex2.x - vertex1.x;
    Qy = vertex2.y - vertex1.y;
    Qz = vertex2.z - vertex1.z;
    Px = vertex3.x - vertex1.x;
    Py = vertex3.y - vertex1.y;
    Pz = vertex3.z - vertex1.z;
    normal->x = Py*Qz - Pz*Qy;
    normal->y = Pz*Qx - Px*Qz;
    normal->z = Px*Qy - Py*Qx;
}

/* Generates the frame of the building, assigning it to the build argument provided */

