//
//  wind.hpp
//  COMP308_a4
//
//  Created by Kalo Pilato on 14/10/15.
//
//


#include "comp308.hpp"

using namespace comp308;

class Wind{
private:
    int frames = 30;
    int currentFrame = 0;
    vec3 noWind = vec3(0.0, 0.0, 0.0);
    vec3 currentWind = noWind;
    vec3 targetWind;
    vec3 windIncrement;
    
public:
    Wind();
    void setWind(vec3 windVector);
    vec3 wind();
    void reset();
};