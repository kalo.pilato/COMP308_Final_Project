//---------------------------------------------------------------------------
//
// Copyright (c) 2015 Taehyun Rhee, Joshua Scott, Ben Allen
//
// This software is provided 'as-is' for assignment of COMP308 in ECS,
// Victoria University of Wellington, without any express or implied warranty. 
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// The contents of this file may not be copied or duplicated in any form
// without the prior permission of its owner.
//
//----------------------------------------------------------------------------

#pragma once

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "comp308.hpp"

struct vertex {
	int p = 0; // index for point in m_points
	int t = 0; // index for uv in m_uvs
	int n = 0; // index for normal in m_normals
};

struct triangle {
	vertex v[3]; // requires 3 verticies
};

struct window {
    comp308::vec3 w[4]; // requires 4 vertices
    comp308::vec2 uv[4];
    comp308::vec3 n;
};

struct building_edge {
    comp308::vec3 v[4];
    comp308::vec2 uv[4];
    comp308::vec3 n;
    bool isTriangle = false; // false = 4 vertices. true = 3 vertices
    std::vector<window> windows;
    float width;
    float height;
    float depth;
};

struct building {
    bool hasSpecialtyRoof = false;
    building_edge e[4]; // each building requires  edges
    // 0 = front || 1 = right || 2 = left || 3 = back || 4 = top
    int stories;
    int roofTexture = -1;
    int windowTexture = -1;
    int wallTexture = -1;
    building_edge sr[4];
    building_edge r;
    
    int building_type = -1; // 0 = residental, 1 = commercial, 2 = city centre
};

struct road {
    comp308::vec3 r[4];
    comp308::vec2 uv[4];
    char orientation = 'n';
    int roadTexture = -1;
};


struct lot {
    comp308::vec3 l[4];
    float width;
    float depth;
    int distanceFromCentre;
    building lotBuilding; // for now, a lot can only have one lot
};

struct suburb {
    int minHeight;
    int maxHeight;
    std::vector<lot> lots;
};

struct city {
    float cityWidth;
    float cityDepth;
    comp308::vec3 c[4];
    std::vector<road> roads;
    comp308::vec2 uv[4];
    int grassTexture = 0;
};

class City {
private:
    int RESIDENTAL_CODE = 0;
    int COMMERCIAL_CODE = 1;
    int HEAVY_COMMERCIAL_CODE = 2;
    
    city generated_city;
    
    void generateCityFloor();
    void generateRoads(float lotWidth, float lotDeep, float roadXLength, float roadZLength, char vertOrHoro);
    void generateLots(float lotWidth, float lotDeep, float roadWidth);
    
    void computeCityDetails();
    void computeSuburbRanges(int maxDistance, std::vector<int> *suburbRanges);
    
    void generateBuildings();
    void generateBuilding(float x, float z, float width, float height, float depth, building *build);
    void generateBuildingWindows(building *building);
    void generateEdgeWindows(float width, float height, float depth, int nHorozontal, int nVertical, float facingExtension, building_edge edge, std::vector<window> *windows);
    int generateBuildingHeight(int suburb);
    void generateBuildingWidthAndDepth(int suburb, float lotWidth, float lotDepth, float *width, float *depth);
    void calculateVectorNormal(comp308::vec3 vertex1, comp308::vec3 vertex2, comp308::vec3 vertex3, comp308::vec3 *normal);
    
    void renderSuburb(std::vector<lot> lots);
    
    GLuint getWallTexture(building build);
    GLuint getWindowTexture(building build);
    GLuint getRoofTexture(building build);
    
    void constructEdges();
    void getSuburbEdges(std::vector<lot> lots, std::vector<window> *allWindows);
    void edgeToWindow(building_edge edge, window *window);
   
    
    void generateGrassUVPoints();
    void generateRoadUVPoints();
public:
	
    
    City(std::vector<GLuint> providedResidentalTextures, std::vector<GLuint> providedCommercialTextures, std::vector<GLuint> providedCityCentreTextures, std::vector<GLuint> providedCityTextures);
	void renderCity();
	std::vector<window> getFaces(); // made public
	
};